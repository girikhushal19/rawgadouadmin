import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginauthenticationService {

  token:any;user_type:any;loggedInDetail:any;user_id:any;
  adminUserLoginCheck = "https://mainserver.rawgadou.com/api/adminUserLoginCheck";
  base_url = "https://administrateur.rawgadou.com/";
  base_url_node_admin = "https://mainserver.rawgadou.com/api/";
  base_url_node = "https://mainserver.rawgadou.com/";
  baseapiurl= "https://mainserver.rawgadou.com/admin-panel/"
  baseapiurl2="https://mainserver.rawgadou.com/"

  // base_url = "http://localhost:4202/";
  // base_url_node_admin = "http://localhost:9001/api/";
  // base_url_node = "http://localhost:9001/";
  // baseapiurl= "http://localhost:9001/admin-panel/";
  // baseapiurl2="http://localhost:9001/";

  constructor() { }

  userLogin()
  {
    this.token = localStorage.getItem("token");
    return this.token;
  }    
  
  userLoggedInType()
  {
    this.user_type = localStorage.getItem("user_type");
    return this.user_type;
  }
  userLoggedIn()
  {
    this.user_id = localStorage.getItem("_id");
    return this.user_id;
  }

}
