import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-all-event-category',
  templateUrl: './all-event-category.component.html',
  styleUrls: ['./all-event-category.component.css']
})
export class AllEventCategoryComponent implements OnInit {
  searchText:any;
  base_url:any; 
  page_no:number=0;numbers:any;selectedIndex: number=0;
  vendorsarray:any;base_url_two:string="";apiResponse:any;
  vendors:any;deletemultipleseller:string="";
  invoice_id:any[] = [];
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService,private http: HttpClient) {
    this.base_url = loginAuthObj.baseapiurl
    this.vendorsarray =[]
    this.vendors = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    //this.deletemultipleseller = this.base_url_two+"api/seller/deletemultipleseller";
    
    this.http.post(this.base_url_two+"api/event/showcatagories",{"pageno":null}).subscribe(res => {
      this.apiResponse = res;
      //console.log(this.apiResponse);
       //this.vendors = this.apiResponse.data;
       this.page_no = this.apiResponse.data;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
    });

    this.getallColor(0)
   }

  ngOnInit(): void {
    

  }
  getallColor(pageno:number=0)
  {
    this.selectedIndex = pageno;
      let queryParam = {pageno:pageno};
    this.http.post(this.base_url_two+"api/event/showcatagories",queryParam).subscribe(res => {
      this.apiResponse = res;
      console.log(" here 46 --->>>>> "+JSON.stringify(this.apiResponse));
       this.vendors = this.apiResponse.data;
      

      
    })
  }
  select_all = false;
  data: any[] = []
}
