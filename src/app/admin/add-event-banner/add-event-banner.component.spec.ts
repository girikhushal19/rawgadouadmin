import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEventBannerComponent } from './add-event-banner.component';

describe('AddEventBannerComponent', () => {
  let component: AddEventBannerComponent;
  let fixture: ComponentFixture<AddEventBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEventBannerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEventBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
