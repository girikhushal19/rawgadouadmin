import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {Location} from '@angular/common';
@Component({
  selector: 'app-editvendor',
  templateUrl: './editvendor.component.html',
  styleUrls: ['./editvendor.component.css']
})
export class EditvendorComponent implements OnInit {
  base_url: any;
  usersarray: any;old_userImage:string="";base_url_two:string="";
  myFiles:string [] = [];formData:any;apiResponse:any;
  users: any;user_id:any;
  form = new UntypedFormGroup({
    images: new UntypedFormControl('', []),
    email: new UntypedFormControl('', [Validators.required]),
    fullname: new UntypedFormControl('', [Validators.required]),
    id: new UntypedFormControl('', [Validators.required]),
    phone: new UntypedFormControl('', [Validators.required]),
    shopname: new UntypedFormControl('', [Validators.required]),
    //pickupaddress: new UntypedFormControl('', [Validators.required]),
    /*account_name: new UntypedFormControl('', [Validators.required]),
    account_number: new UntypedFormControl('', [Validators.required]),
    bank_name: new UntypedFormControl('', [Validators.required]),
    bank_address: new UntypedFormControl('', [Validators.required]),
    iban :new UntypedFormControl('', [Validators.required]),
    bic: new UntypedFormControl('', [Validators.required]),*/
  });
  constructor(private http: HttpClient, private activeRoute: ActivatedRoute, private loginAuthObj: LoginauthenticationService,
    private router: Router,private _location: Location) {

      //console.log("dfdsfdf user id "+this.activeRoute.snapshot.params['id'])
    this.user_id = this.activeRoute.snapshot.params['id'];
    this.base_url = loginAuthObj.baseapiurl
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.users = []
    this.usersarray = []
  }

  ngOnInit(): void {
    this.myFiles = [];
    

    //{{local}}/63f5c1ec5bf0bda586960971

    this.http.get(this.base_url_two + "api/seller/getuserprofile/"+this.user_id).subscribe(res => {
      //console.log("api response "+res);
      this.usersarray = res
      this.users = this.usersarray.data
      console.log("this.users  "+ JSON.stringify(this.users));
      const user = this.usersarray.data
      if (user) {
        if(user.photo)
        {
          this.old_userImage = user.photo;
        }

        this.form = new UntypedFormGroup({
          images: new UntypedFormControl('', []),
          email: new UntypedFormControl(user.email, [Validators.required]),
          fullname: new UntypedFormControl(user.fullname, [Validators.required]),
          id: new UntypedFormControl(this.activeRoute.snapshot.params['id'], [Validators.required]),
          phone: new UntypedFormControl(user.phone, [Validators.required]),
          shopname: new UntypedFormControl(user.shopname, [Validators.required]),
          pickupaddress: new UntypedFormControl(user.pickupaddress, [Validators.required]),
          /*account_name: new UntypedFormControl(user.account_name, [Validators.required]),
          account_number: new UntypedFormControl(user.account_number, [Validators.required]),
          bank_name: new UntypedFormControl(user.bank_name, [Validators.required]),
          bank_address: new UntypedFormControl(user.bank_address, [Validators.required]),
          iban: new UntypedFormControl(user.iban, [Validators.required]),
          bic: new UntypedFormControl(user.bic, [Validators.required]),*/
        });

      }
    })
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit() {
    console.log(this.form.value)
    this.formData = new FormData(); 
    this.formData.append('fullname', this.form.value.fullname);
    this.formData.append('id', this.form.value.id);
    this.formData.append('email', this.form.value.email);
    this.formData.append('phone', this.form.value.phone);
    this.formData.append('shopname', this.form.value.shopname);
    this.formData.append('pickupaddress', this.form.value.pickupaddress);
    /*this.formData.append('account_name', this.form.value.account_name);
    this.formData.append('account_number', this.form.value.account_number);
    this.formData.append('bank_name', this.form.value.bank_name);
    this.formData.append('bank_address', this.form.value.bank_address);
    this.formData.append('iban', this.form.value.iban);
    this.formData.append('bic', this.form.value.bic);*/
    
  //this.formData.append('file', this.images);

    for (var i = 0; i < this.myFiles.length; i++)
    { 
      this.formData.append("photo", this.myFiles[i]);
    } 
    console.log(this.formData);
    this.http.post(this.base_url + "updatevendors", this.formData).subscribe(res => {
      console.log(res);
      //this.router.navigate(['sellers'])
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          //this.router.navigate(['sellers'])
          this._location.back();
        }, 1000); 
      }
    })
  }

}
