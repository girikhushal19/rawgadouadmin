import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEventBannerComponent } from './all-event-banner.component';

describe('AllEventBannerComponent', () => {
  let component: AllEventBannerComponent;
  let fixture: ComponentFixture<AllEventBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllEventBannerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllEventBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
