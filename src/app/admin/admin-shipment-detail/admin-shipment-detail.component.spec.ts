import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminShipmentDetailComponent } from './admin-shipment-detail.component';

describe('AdminShipmentDetailComponent', () => {
  let component: AdminShipmentDetailComponent;
  let fixture: ComponentFixture<AdminShipmentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminShipmentDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminShipmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
