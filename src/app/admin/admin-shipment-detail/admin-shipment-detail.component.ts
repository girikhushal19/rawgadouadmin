import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common'; 


@Component({
  selector: 'app-admin-shipment-detail',
  templateUrl: './admin-shipment-detail.component.html',
  styleUrls: ['./admin-shipment-detail.component.css']
})
export class AdminShipmentDetailComponent implements OnInit {
  order_id: string;
  base_url = "";base_url_node = ""; attributeArrayValue:any;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editRestaurantSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getAdminShipmentDetail:any;getqueryParam:any;
  base_url_node_only:any;record:any; 
  centerLatitude = 47.034577;
  centerLongitude = 1.156914;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
     

    this.order_id = this.actRoute.snapshot.params['id'];
    this.getqueryParam = {"order_id":this.order_id};
    this.getAdminShipmentDetail = this.base_url_node+"getAdminShipmentDetail"; 

    this._http.post(this.getAdminShipmentDetail,this.getqueryParam).subscribe((response:any)=>{
      
      this.record = response.record;
      console.log("this record "+ JSON.stringify(this.record));
      if(this.record)
      {
        if(this.record[0].pickup_location)
        {
          if(this.record[0].pickup_location.coordinates)
          {
            if(this.record[0].pickup_location.coordinates.length > 0)
            {
              console.log("pickup_location -> "+this.record[0].pickup_location.coordinates[0]);
              console.log("pickup_location -> "+this.record[0].pickup_location.coordinates[1]);
              this.centerLatitude = this.record[0].pickup_location.coordinates[0];
              this.centerLongitude = this.record[0].pickup_location.coordinates[1];
              console.log(this.centerLatitude);
              console.log(this.centerLongitude);
              this.marker = {
                position: { lat: this.centerLatitude, lng: this.centerLongitude },
              }
            }
          }
        }
        if(this.record[0].receiver_location)
        {
          if(this.record[0].receiver_location.coordinates)
          {
            if(this.record[0].receiver_location.coordinates.length > 0)
            {
              console.log("receiver_location -> "+this.record[0].receiver_location.coordinates[0]);
              console.log("receiver_location -> "+this.record[0].receiver_location.coordinates[1]);
              this.centerLatitude = this.record[0].receiver_location.coordinates[0];
              this.centerLongitude = this.record[0].receiver_location.coordinates[1];
              console.log(this.centerLatitude);
              console.log(this.centerLongitude);
              this.marker2 = {
                position: { lat: this.centerLatitude, lng: this.centerLongitude },
              }
            }
          }
        }
        if(this.record[0].sender_location)
        {
          if(this.record[0].sender_location.coordinates)
          {
            if(this.record[0].sender_location.coordinates.length > 0)
            {
              console.log("sender_location -> "+this.record[0].sender_location.coordinates[0]);
              console.log("sender_location -> "+this.record[0].sender_location.coordinates[1]);
              this.centerLatitude = this.record[0].sender_location.coordinates[0];
              this.centerLongitude = this.record[0].sender_location.coordinates[1];
              console.log(this.centerLatitude);
              console.log(this.centerLongitude);
              this.marker3 = {
                position: { lat: this.centerLatitude, lng: this.centerLongitude },
              }
            }
          }
        }


      }
    });
  }
  ngOnInit(): void {
   
  }

  mapOptions: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }


  mapOptions2: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker2 = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }
  mapOptions3: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker3 = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }
}
