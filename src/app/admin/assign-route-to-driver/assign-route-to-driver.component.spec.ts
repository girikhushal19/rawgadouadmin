import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRouteToDriverComponent } from './assign-route-to-driver.component';

describe('AssignRouteToDriverComponent', () => {
  let component: AssignRouteToDriverComponent;
  let fixture: ComponentFixture<AssignRouteToDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignRouteToDriverComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AssignRouteToDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
