import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllInActiveUsersComponent } from './all-in-active-users.component';

describe('AllInActiveUsersComponent', () => {
  let component: AllInActiveUsersComponent;
  let fixture: ComponentFixture<AllInActiveUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllInActiveUsersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllInActiveUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
