import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipingareaComponent } from './shipingarea.component';

describe('ShipingareaComponent', () => {
  let component: ShipingareaComponent;
  let fixture: ComponentFixture<ShipingareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShipingareaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShipingareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
