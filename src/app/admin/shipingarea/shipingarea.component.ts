import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { AddadvertiseComponent } from '../addadvertise/addadvertise.component';
import { AddshiipingareaComponent } from '../addshiipingarea/addshiipingarea.component';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
@Component({
  selector: 'app-shipingarea',
  templateUrl: './shipingarea.component.html',
  styleUrls: ['./shipingarea.component.css']
})
export class ShipingareaComponent implements OnInit {
  base_url:any;apiResponse:any;records:any;
  constructor(public dialog: MatDialog,private http: HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.base_url = loginAuthObj.baseapiurl2
  }

  ngOnInit(): void {
    this.http.get(this.base_url+"api/shipping/geAlltZones").subscribe(res=>{
      console.log(res)
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
        this.records = this.apiResponse.data;
        console.log(this.records);
      }
    })
    
  }
  select_all = false;
  data: any[] = [
  ]
  onSelectAll(e: any): void { 
    for (let i = 0; i < this.data.length; i++) {
      const item = this.data[i];
      item.is_selected = e;
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddshiipingareaComponent ,{
      height: '600px',
      width: '900px'
    });
  }

}
