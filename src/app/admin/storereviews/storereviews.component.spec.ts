import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorereviewsComponent } from './storereviews.component';

describe('StorereviewsComponent', () => {
  let component: StorereviewsComponent;
  let fixture: ComponentFixture<StorereviewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorereviewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StorereviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
