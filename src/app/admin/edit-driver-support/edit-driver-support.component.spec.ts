import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDriverSupportComponent } from './edit-driver-support.component';

describe('EditDriverSupportComponent', () => {
  let component: EditDriverSupportComponent;
  let fixture: ComponentFixture<EditDriverSupportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDriverSupportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditDriverSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
