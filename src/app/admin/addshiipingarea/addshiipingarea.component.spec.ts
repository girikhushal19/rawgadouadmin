import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddshiipingareaComponent } from './addshiipingarea.component';

describe('AddshiipingareaComponent', () => {
  let component: AddshiipingareaComponent;
  let fixture: ComponentFixture<AddshiipingareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddshiipingareaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddshiipingareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
