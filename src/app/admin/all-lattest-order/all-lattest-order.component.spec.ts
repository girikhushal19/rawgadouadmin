import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllLattestOrderComponent } from './all-lattest-order.component';

describe('AllLattestOrderComponent', () => {
  let component: AllLattestOrderComponent;
  let fixture: ComponentFixture<AllLattestOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllLattestOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllLattestOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
