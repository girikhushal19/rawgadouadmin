import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-get-vendor-payment-request',
  templateUrl: './get-vendor-payment-request.component.html',
  styleUrls: ['./get-vendor-payment-request.component.css']
})
export class GetVendorPaymentRequestComponent implements OnInit {
  searchText:any;
  base_url:any;
  apiResponse:any;base_url_two:string="";apiResponse_2:any;
  record:any;deletemultipleseller:string="";
  invoice_id:any[] = [];id:any;
   base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any; formValue:any; totalPageNumber:any;allRoutePlan:any;queryParam:any;numbers:any;allRoutePlanCount:any;apiStringify:any;updateRoutePlanStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;
  selectedIndex: number;deleteRoutePlan:any;
  constructor(private loginAuthObj:LoginauthenticationService,private http: HttpClient,private actRoute: ActivatedRoute) {
    this.base_url = loginAuthObj.baseapiurl
    this.record = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allRoutePlanCount = this.base_url_two+"api/seller/getCountPaymentRequestVendor";
    this.allRoutePlan = this.base_url_two+"api/seller/getPaymentRequestVendor";

    this.id = this.actRoute.snapshot.params['id'];

    this.queryParam = {"seller_id":this.id};
    this.http.post(this.allRoutePlanCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allRoutePlanCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });

    
    this.getallBanner(0);
    this.selectedIndex = 0;
   }


  ngOnInit(): void { }


  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage,"seller_id":this.id};

    this.http.post(this.base_url_two+"api/seller/getPaymentRequestVendor",this.queryParam).subscribe(res => {
      this.apiResponse = res;
      if(this.apiResponse.status == true )
      {
        this.record =this.apiResponse.data;
      }
    });
  }
  markPaymentDone(t_id:any,val:string="")
  {
    console.log("id ",t_id);
    console.log("val ",val);
    if(val == 'd')
    {
      //markpaymentrequestasresolved
      this.queryParam = {"id":t_id};

      this.http.get(this.base_url_two+"api/seller/markpaymentrequestasresolved/"+t_id).subscribe(res => {
        this.apiResponse_2 = res;
        if(this.apiResponse_2.status == true )
        {
          setTimeout(()=>{
            window.location.reload();
          },2000);
        }
      });
    }else{
      this.queryParam = {"id":t_id};

      this.http.get(this.base_url_two+"api/seller/markpaymentrequestascanclled/"+t_id).subscribe(res => {
        this.apiResponse_2 = res;
        if(this.apiResponse_2.status == true )
        {
          setTimeout(()=>{
            window.location.reload();
          },2000);
        }
      });
    }
  }
}
