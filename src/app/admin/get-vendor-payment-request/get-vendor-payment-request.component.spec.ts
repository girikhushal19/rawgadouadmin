import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetVendorPaymentRequestComponent } from './get-vendor-payment-request.component';

describe('GetVendorPaymentRequestComponent', () => {
  let component: GetVendorPaymentRequestComponent;
  let fixture: ComponentFixture<GetVendorPaymentRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetVendorPaymentRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetVendorPaymentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
