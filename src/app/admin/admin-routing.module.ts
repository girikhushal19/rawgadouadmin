import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from './logout/logout.component';
import { DemoRoutePlanComponent } from './demo-route-plan/demo-route-plan.component';
import { DemoRoutePlanNewComponent } from './demo-route-plan-new/demo-route-plan-new.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { DemoTimePickerComponent } from './demo-time-picker/demo-time-picker.component';
import { AllDriversComponent } from './all-drivers/all-drivers.component';
import { EditDriversComponent } from './edit-drivers/edit-drivers.component';
import { AddRouteComponent } from './add-route/add-route.component';
import { AllRouteComponent } from './all-route/all-route.component';
import { EditRouteComponent } from './edit-route/edit-route.component';
import { AssignRouteComponent } from './assign-route/assign-route.component';
import { AssignRouteToDriverComponent } from './assign-route-to-driver/assign-route-to-driver.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { UsersComponent } from './users/users.component';
import { VendormanagementComponent } from './vendormanagement/vendormanagement.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { SellersComponent } from './sellers/sellers.component';
import { AbusereportComponent } from './abusereport/abusereport.component';
import { StorereviewsComponent } from './storereviews/storereviews.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { RefundsComponent } from './refunds/refunds.component';
import { ReportsComponent } from './reports/reports.component';
import { AllproductsComponent } from './allproducts/allproducts.component';
import { CategoriesComponent } from './categories/categories.component';
import { OrdersComponent } from './orders/orders.component';
import { AddplanComponent } from './addplan/addplan.component';
import { StatisticsOComponent } from './statistics-o/statistics-o.component';
import { OrderreportComponent } from './orderreport/orderreport.component';
import { ShipingareaComponent } from './shipingarea/shipingarea.component';
import { MarkerplaceComponent } from './markerplace/markerplace.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { EditvendorComponent } from './editvendor/editvendor.component';
import { ModifyproductComponent } from './modifyproduct/modifyproduct.component';
import { EditStopComponent } from './edit-stop/edit-stop.component';
import { ShippingLabelComponent } from './shipping-label/shipping-label.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';

import { AllMerchantComponent } from './all-merchant/all-merchant.component';
import { AllShipmentComponent } from './all-shipment/all-shipment.component';
import { AllClientComponent } from './all-client/all-client.component';
import { AdminClientDetailComponent } from './admin-client-detail/admin-client-detail.component';
import { AdminShipmentDetailComponent } from './admin-shipment-detail/admin-shipment-detail.component';
import { EditDriverSupportComponent } from './edit-driver-support/edit-driver-support.component';
import { AdminSettingsComponent } from './admin-settings/admin-settings.component';
import { SendPushNotificationMerchantComponent } from './send-push-notification-merchant/send-push-notification-merchant.component';
import { AllPushNotificationComponent } from './all-push-notification/all-push-notification.component';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { AllInvoiceComponent } from './all-invoice/all-invoice.component';
import { AllInvoiceShipmentComponent } from './all-invoice-shipment/all-invoice-shipment.component';
import { AllCategoryComponent } from './all-category/all-category.component';

import { AllMarketPlaceComponent } from './all-market-place/all-market-place.component';
import { GetVendorBanksComponent } from './get-vendor-banks/get-vendor-banks.component';
import { EditBankDetailComponent } from './edit-bank-detail/edit-bank-detail.component';
import { OrderListComponent } from './order-list/order-list.component';
import { AddOrganizerComponent } from './add-organizer/add-organizer.component';
import { AllOrganizerComponent } from './all-organizer/all-organizer.component';
import { EditOrganizerComponent } from './edit-organizer/edit-organizer.component';
import { AddEventCategoryComponent } from './add-event-category/add-event-category.component';
import { AllEventCategoryComponent } from './all-event-category/all-event-category.component';
import { EditEventCategoryComponent } from './edit-event-category/edit-event-category.component';
import { AddEventComponent } from './add-event/add-event.component';
import { AllEventComponent } from './all-event/all-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { SellerTermsConditionComponent } from './seller-terms-condition/seller-terms-condition.component';

import { EditMarcketPlaceComponent } from './edit-marcket-place/edit-marcket-place.component';
import { EditZoneNameComponent } from './edit-zone-name/edit-zone-name.component';
import { EditPlanComponent } from './edit-plan/edit-plan.component';
import { EditWebPagesComponent } from './edit-web-pages/edit-web-pages.component';


import { AddBannerMiddleComponent } from './add-banner-middle/add-banner-middle.component';
import { AllBannerMiddleComponent } from './all-banner-middle/all-banner-middle.component';
import { EditBannerMiddleComponent } from './edit-banner-middle/edit-banner-middle.component';

import { AddEventBannerComponent } from './add-event-banner/add-event-banner.component';
import { AllEventBannerComponent } from './all-event-banner/all-event-banner.component';
import { EditEventBannerComponent } from './edit-event-banner/edit-event-banner.component';

import { AllExpiredEventComponent } from './all-expired-event/all-expired-event.component';
import { AllOngoingEventComponent } from './all-ongoing-event/all-ongoing-event.component';
import { AllOrganizerEarningComponent } from './all-organizer-earning/all-organizer-earning.component';
import { MakePaymentRequestComponent } from './make-payment-request/make-payment-request.component';
import { OrganiserPaymentHistoryComponent } from './organiser-payment-history/organiser-payment-history.component';
import { ChangeMerchantCreditComponent } from './change-merchant-credit/change-merchant-credit.component';
import { GetVendorPaymentRequestComponent } from './get-vendor-payment-request/get-vendor-payment-request.component';
import { AllEventBookingComponent } from './all-event-booking/all-event-booking.component';

import { ActiveSellerComponent } from './active-seller/active-seller.component';
import { InActiveSellerComponent } from './in-active-seller/in-active-seller.component';
import { DeletedSellerComponent } from './deleted-seller/deleted-seller.component';
import { AllActiveUsersComponent } from './all-active-users/all-active-users.component';
import { AllInActiveUsersComponent } from './all-in-active-users/all-in-active-users.component';
import { AllDeletedUsersComponent } from './all-deleted-users/all-deleted-users.component';
import { AllStopComponent } from './all-stop/all-stop.component';
import { AllInactiveProductComponent } from './all-inactive-product/all-inactive-product.component';
import { AllActiveProductComponent } from './all-active-product/all-active-product.component';
import { AllDeletedProductComponent } from './all-deleted-product/all-deleted-product.component';


import { AllLattestOrderComponent } from './all-lattest-order/all-lattest-order.component';


import { AllRadyToPickUpOrderComponent } from './all-rady-to-pick-up-order/all-rady-to-pick-up-order.component';
import { AllPickedUpOrderComponent } from './all-picked-up-order/all-picked-up-order.component';
import { AllCompletedOrderComponent } from './all-completed-order/all-completed-order.component';
import { AllUserCancledOrderComponent } from './all-user-cancled-order/all-user-cancled-order.component';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';

const routes: Routes = [
  {
    path:"",
    component:LoginComponent
  },
  {
    path:"dashboard",
    component:DashboardComponent
  },
  {
    path:"logout",
    component:LogoutComponent
  },
  {
    path:"demoRoutePlan",
    component:DemoRoutePlanComponent
  },
  {
    path:"demoRoutePlanNew",
    component:DemoRoutePlanNewComponent
  },
  {
    path:"addDriver",
    component:AddDriverComponent
  },
  {
    path:"allDrivers",
    component:AllDriversComponent
  },
  {
    path:"editDriver/:id",
    component:EditDriversComponent
  },
  {
    path:"addTimePicker",
    component:DemoTimePickerComponent
  },
  {
    path:"addRoute",
    component:AddRouteComponent
  },
  {
    path:"addRoute/:id",
    component:AddRouteComponent
  },
  {
    path:"allRoute",
    component:AllRouteComponent
  },
  {
    path:"assignRoute/:id",
    component:AssignRouteComponent
  },
  {
    path:"assignRouteToDriver/:route_id/:driver_id",
    component:AssignRouteToDriverComponent
  },
  {
    path:"statistics_market",
    component:StatisticsComponent
  },
  {
    path:"users",
    component:UsersComponent
  },
  {
    path:"vendor_management",
    component:VendormanagementComponent
  },
  {
    path:"withdraw",
    component:WithdrawComponent
  },
  {
    path:"sellers",
    component:SellersComponent
  },
  {
    path:"abuse_report",
    component:AbusereportComponent
  },
  {
    path:"store_reviews",
    component:StorereviewsComponent
  },
  {
    path:"advertisement",
    component:AdvertisementComponent
  },
  {
    path:"refunds",
    component:RefundsComponent
  },
  {
    path:"reports",
    component:ReportsComponent
  },
  {
    path:"all_products",
    component:AllproductsComponent
  },
  {
    path:"categories",
    component:CategoriesComponent
  },
  {
    path:"orders",
    component:OrdersComponent
  },
  {
    path:"allLattestOrder",
    component:AllLattestOrderComponent
  },
  {
    path:"allReadyToPickupOrder",
    component:AllRadyToPickUpOrderComponent
  },
  {
    path:"allPickedUpOrder",
    component:AllPickedUpOrderComponent
  },
  {
    path:"allCompletedOrder",
    component:AllCompletedOrderComponent
  },
  
  {
    path:"allUserCancledOrder",
    component:AllUserCancledOrderComponent
  },
  
  {
    path:"add_plan",
    component:AddplanComponent
  },
  {
    path:"statistics_plan",
    component:StatisticsOComponent
  },
  {
    path:"order_report",
    component:OrderreportComponent
  },
  {
    path:"shipping_area",
    component:ShipingareaComponent
  },
  {
    path:"market_place",
    component:MarkerplaceComponent
  },
  {
    path:"editprofile/:id",
    component:EditprofileComponent
  },
  {
    path:"editvendor/:id",
    component:EditvendorComponent
  },
  {
    path:"modifyproduct/:id",
    component:ModifyproductComponent
  },
  {
    path:"editStop/:id",
    component:EditStopComponent
  },
  {
    path:"shipping_label/:id",
    component:ShippingLabelComponent
  },
  {
    path:"allMerchant",
    component:AllMerchantComponent
  },
  {
    path:"allShipment",
    component:AllShipmentComponent
  },
  {
    path:"allClient/:id",
    component:AllClientComponent
  },
  {
    path:"adminClientDetail/:id",
    component:AdminClientDetailComponent
  },
  {
    path:"adminShipmentDetail/:id",
    component:AdminShipmentDetailComponent
  },
  {
    path:"editDriverSupport",
    component:EditDriverSupportComponent
  },
  {
    path:"adminSetting",
    component:AdminSettingsComponent
  },
  {
    path:"sendPushNotificationMerchant",
    component:SendPushNotificationMerchantComponent
  },
  {
    path:"allPushNotification",
    component:AllPushNotificationComponent
  },
  {
    path:"addInvoice",
    component:AddInvoiceComponent
  },
  {
    path:"allInvoice",
    component:AllInvoiceComponent
  },
  {
    path:"allInvoiceShipment/:id",
    component:AllInvoiceShipmentComponent
  },
  {
    path:"allCategory",
    component:AllCategoryComponent
  },
  {
    path:"allMarketPlace",
    component:AllMarketPlaceComponent
  },
  {
    path:"editMarcketPlace/:id",
    component:EditMarcketPlaceComponent
  },
  {
    path:"getVendorBanks/:id",
    component:GetVendorBanksComponent
  },
  {
    path:"editBankDetail/:id/:bank_id",
    component:EditBankDetailComponent
  },
  {
    path:"orderList",
    component:OrderListComponent
  },
  {
    path:"addOrganizer",
    component:AddOrganizerComponent
  },
  {
    path:"allOrganizer",
    component:AllOrganizerComponent
  },
  {
    path:"editOrganizer/:id",
    component:EditOrganizerComponent
  },
  {
    path:"addEventCategory",
    component:AddEventCategoryComponent
  },
  {
    path:"allEventCategory",
    component:AllEventCategoryComponent
  },
  {
    path:"editEventCategory/:id",
    component:EditEventCategoryComponent
  },
  {
    path:"addEvent",
    component:AddEventComponent
  },
  {
    path:"allEvent",
    component:AllEventComponent
  },
  {
    path:"editEvent/:id",
    component:EditEventComponent
  },
  {
    path:"orderDetail/:id",
    component:OrderDetailComponent
  },
  {
    path:"editCategory/:id",
    component:EditCategoryComponent
  },
  {
    path:"sellerTermsCondition",
    component:SellerTermsConditionComponent
  },
  {
    path:"editZoneName/:id",
    component:EditZoneNameComponent
  },
  {
    path:"editPlan/:id",
    component:EditPlanComponent
  },
  {
    path:"editWebPages",
    component:EditWebPagesComponent
  },
  {
    path:"addBannerMiddle",
    component:AddBannerMiddleComponent
  },
  {
    path:"allBannerMiddle",
    component:AllBannerMiddleComponent
  },
  {
    path:"editBannerMiddle/:id",
    component:EditBannerMiddleComponent
  },

  {
    path:"addEventBanner",
    component:AddEventBannerComponent
  },
  {
    path:"allEventBanner",
    component:AllEventBannerComponent
  },
  {
    path:"editEventBanner/:id",
    component:EditEventBannerComponent
  },
  {
    path:"allEventExpired",
    component:AllExpiredEventComponent
  },
  {
    path:"allEventOngoing",
    component:AllOngoingEventComponent
  },
  {
    path:"allOrganizerEarning",
    component:AllOrganizerEarningComponent
  },
  {
    path:"makePaymentRequest/:id",
    component:MakePaymentRequestComponent
  },
  {
    path:"organiserPaymentHistory/:id",
    component:OrganiserPaymentHistoryComponent
  },
  {
    path:"changeMerchantCredit/:id",
    component:ChangeMerchantCreditComponent
  },
  {
    path:"getVendorPaymentRequest/:id",
    component:GetVendorPaymentRequestComponent
  },
  {
    path:"allEventBooking/:id",
    component:AllEventBookingComponent
  },
  {
    path:"activeSeller",
    component:ActiveSellerComponent
  },
  {
    path:"inActiveSeller",
    component:InActiveSellerComponent
  },
  {
    path:"deletedSeller",
    component:DeletedSellerComponent
  },
  {
    path:"allActiveUsers",
    component:AllActiveUsersComponent
  },
  {
    path:"allInActiveUsers",
    component:AllInActiveUsersComponent
  },
  {
    path:"allDeletedUsers",
    component:AllDeletedUsersComponent
  },
  {
    path:"allStop/:id",
    component:AllStopComponent
  },
  {
    path:"allInactiveProduct",
    component:AllInactiveProductComponent
  },
  {
    path:"allActiveProduct",
    component:AllActiveProductComponent
  },
  {
    path:"allDeletedProduct",
    component:AllDeletedProductComponent
  },
  {
    path:"editAdminProfile",
    component:EditAdminProfileComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
