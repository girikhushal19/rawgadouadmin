import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-add-invoice',
  templateUrl: './add-invoice.component.html',
  styleUrls: ['./add-invoice.component.css']
})
export class AddInvoiceComponent implements OnInit {

  invoice_id:any[] = [];
  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allCompletedShipment:any;queryParam:any;numbers:any;allCompletedShipmentCount:any;apiStringify:any;updateDriverStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;allMerchant:any;
  selectedIndex: number;allMerchantForInvoice:any;generateInvoiceApi:any;
  //dtOptions: DataTables.Settings = {}; 


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    //console.log(this.base_url_node);


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
       window.location.href = this.base_url;
    }

    this.exportCsvDriverApi = this.base_url_node+"exportCsvDriverApi";
    this.allCompletedShipmentCount = this.base_url_node+"allCompletedShipmentCount";
    this.allCompletedShipment = this.base_url_node+"allCompletedShipment";
    this.updateDriverStatusApi = this.base_url_node+"updateDriverStatusApi";
    this.generateInvoiceApi = this.base_url_node+"generateInvoiceApi";
    this.allMerchantForInvoice = this.base_url_node+"allMerchantForInvoice";

    this._http.post(this.allMerchantForInvoice,{}).subscribe((response:any)=>{
      console.log(response);
      if(response.error == false)
      {
        this.allMerchant = response.record;
      }
    });

    this.selectedIndex = 0;

  }

  ngOnInit(): void {
    // this.dtOptions = {
    //   "searching": false,
    //   "responsive": true,
    //   "autoWidth": false,
    //   "language": {
	  //   "search": "Recherche",
	  //   //"info": "Recherche",
	  //   //"entries": "Recherche",
	  //   }
    // };
    
  }
  getallBanner(numofpage=0)
  {
    console.log(this.form.value.user_id);
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage,"user_id":this.form.value.user_id};
    this._http.post(this.allCompletedShipment,this.queryParam).subscribe((response:any)=>{
      //console.log("allCompletedShipment"+response);
      
      
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
      
    });
  }
  
  form = new UntypedFormGroup({
    user_id: new UntypedFormControl('', []),
    
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    console.log(this.form.value);
    this._http.post(this.allCompletedShipment,this.form.value).subscribe((response:any)=>{
      //console.log("allCompletedShipment"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  onChange(id:any)
  {
    console.log("id  ---->   "+id);
    this.getallBanner(0)
  }
  select_all = false;
  onSelectAll(e: any): void { 
   //console.log("is checked "+e);
   for (let i = 0; i < this.record.length; i++)
    {
      const item = this.record[i];
      item.is_checked = e;
      
    }

   if(e == true)
   {
    for (let i = 0; i < this.record.length; i++)
    {
      const item = this.record[i];
      item.is_checked = e;
      //console.log(item._id);
      let cc = item._id;
      this.invoice_id.push(cc);
      
    }
   }else{
    this.invoice_id = [];
   }
   console.log("invoiceeee "+this.invoice_id);
  }
  onSelectAll2(id:any,event:any)
  {
    console.log("id"+id);
    console.log("event"+event);
    if(event == true)
    {
      this.invoice_id.push(id);
    }else{
      const index = this.invoice_id.indexOf(id);
      this.invoice_id.splice(index, 1)
    }
    console.log("invoiceeee single select"+this.invoice_id);
  }
  generateInvoice()
  {
    this.queryParam = {"shipment_id":this.invoice_id};
    this._http.post(this.generateInvoiceApi,this.queryParam).subscribe((response:any)=>{
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        //this.form.reset();
        //this.myFiles = [];
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      }

    });
  }
}
