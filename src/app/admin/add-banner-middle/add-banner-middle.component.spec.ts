import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBannerMiddleComponent } from './add-banner-middle.component';

describe('AddBannerMiddleComponent', () => {
  let component: AddBannerMiddleComponent;
  let fixture: ComponentFixture<AddBannerMiddleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBannerMiddleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddBannerMiddleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
