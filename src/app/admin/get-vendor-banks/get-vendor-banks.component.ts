import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-get-vendor-banks',
  templateUrl: './get-vendor-banks.component.html',
  styleUrls: ['./get-vendor-banks.component.css']
})
export class GetVendorBanksComponent implements OnInit {
  searchText:any;
  base_url:any;
  apiResponse:any;base_url_two:string="";
  record:any;deletemultipleseller:string="";
  invoice_id:any[] = [];id:any;
  constructor(private loginAuthObj:LoginauthenticationService,private http: HttpClient,private actRoute: ActivatedRoute) {
    this.base_url = loginAuthObj.baseapiurl
    this.record = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    
    this.id = this.actRoute.snapshot.params['id'];

    this.http.get(this.base_url_two+"api/seller/getallbanks/"+this.id).subscribe(res => {
      this.apiResponse = res
      this.record =this.apiResponse.data;
      
    })

   }

  ngOnInit(): void {

  }

}
