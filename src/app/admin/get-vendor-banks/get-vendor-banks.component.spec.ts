import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetVendorBanksComponent } from './get-vendor-banks.component';

describe('GetVendorBanksComponent', () => {
  let component: GetVendorBanksComponent;
  let fixture: ComponentFixture<GetVendorBanksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetVendorBanksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetVendorBanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
