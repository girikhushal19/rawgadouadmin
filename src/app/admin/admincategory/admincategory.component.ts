import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admincategory',
  templateUrl: './admincategory.component.html',
  styleUrls: ['./admincategory.component.css']
})
export class AdmincategoryComponent implements OnInit {
  toggle: any = {};
  i :any;
  maincategory:any;
  subcaregory:any
  myArray: any = [
    {
      'title': 'one'
    },
    {
      'title': 'two',
      'children': [
        {
          'title': 'two.one'
        },
        {
          'title': 'two.two',
          'children': [
            {
              'title': 'two.two.one'
            },
            {
              'title': 'two.two.two'
            }
          ]
        },
        {
          'title': 'two.three',
          'children': [
            {
              'title': 'two.three.one'
            },
            {
              'title': 'two.three.two'
            }
          ]
        }
      ]
    },
    {
      'title': 'three',
      'children': [
        {
          'title': 'three.one'
        },
        {
          'title': 'three.two',
          'children': [
            {
              'title': 'three.two.one'
            },
            {
              'title': 'three.two.two'
            }
          ]
        },
        {
          'title': 'three.three',
          'children': [
            {
              'title': 'three.three.one'
            },
            {
              'title': 'three.three.two'
            }
          ]
        }
      ]
    },
    {
      'title': 'four'
    },
    {
      'title': 'five',
      'children': [
        {
          'title': 'five.one'
        },
        {
          'title': 'five.two'
        }
      ]
    },
    {
      'title': 'six',
      'children': [
        {
          'title': 'six.one'
        },
        {
          'title': 'six.two',
          'children': [
            {
              'title': 'six.two.one'
            },
            {
              'title': 'six.two.two'
            }
          ]
        },
        {
          'title': 'six.three',
          'children': [
            {
              'title': 'six.three.one'
            },
            {
              'title': 'six.three.two'
            }
          ]
        }
      ]
    }
  ];

  constructor() {
    this.toggle = this.myArray.map((i: any) => false);
   }

  ngOnInit(): void {
  }
  selectone(category:any){
    this.maincategory = category
    this.subcaregory = ''
  }
  selecttwo(category:any){
    this.subcaregory = category
  }
  

}
