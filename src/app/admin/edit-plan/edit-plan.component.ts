import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-edit-plan',
  templateUrl: './edit-plan.component.html',
  styleUrls: ['./edit-plan.component.css']
})
export class EditPlanComponent implements OnInit {
  searchText:any;base_url:string='';obj:any;
  formValue:any;formData:any;
  select_all = false;apiResponse:any;allMainCategory:any;
  oldType:string='a';edit_id:string="";

  old_id: any;
  old_package: any;
  old_plusperproduct: any;
  old_extracommission: any;
  old_price: any;
  old_duration: any;
  old_type: any;
  old_Numberofitems: any;
  old_is_limited: any;
  old_description: any; 


  constructor(private http:HttpClient,private loginAuthObj:LoginauthenticationService,private actRoute: ActivatedRoute, private router: Router)
  {
    this.base_url = this.loginAuthObj.baseapiurl2;
    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.http.get(this.base_url+"api/subs/getSubscriptionById/"+this.edit_id).subscribe((response:any)=>{
      console.log(response);
      this.old_id = response._id;
      this.old_package = response.package;
      this.old_plusperproduct = response.plusperproduct;
      this.old_extracommission = response.extracommission;
      this.old_price = response.price;
      this.old_duration = response.duration;
      //this.old_type = response.type;
      this.old_Numberofitems = response.Numberofitems;
      this.old_is_limited = response.is_limited;
      this.old_description = response.description; 
    });
  }
  
  ngOnInit(): void {
    //console.log("dsfdsfd this.base_url -> "+this.base_url);
    
  }

  
  
  form = new UntypedFormGroup({
    id: new UntypedFormControl('', []),
    package: new UntypedFormControl('', [Validators.required]),
    plusperproduct: new UntypedFormControl('', [Validators.required]),
    extracommission: new UntypedFormControl('', [Validators.required]),
    price: new UntypedFormControl('', [Validators.required]),
    duration: new UntypedFormControl('', [Validators.required]),
    type: new UntypedFormControl('a', []),
    Numberofitems: new UntypedFormControl('', [Validators.required]),
    is_limited: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', []), 
    
  });
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  submit()
  {
    if(this.form.valid)
    {
      console.log(this.form.value);
      this.http.post(this.base_url+"api/subs/createoreditsubpackage",this.form.value).subscribe((response:any)=>{
        console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == 'success')
        {
          setTimeout(() => {
            //window.location.reload();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
