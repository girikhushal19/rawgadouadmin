import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-banner-middle',
  templateUrl: './edit-banner-middle.component.html',
  styleUrls: ['./edit-banner-middle.component.css']
})
export class EditBannerMiddleComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addBannerImageSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;base_url_node_only:any;
  id:any;record:any;old_title:any;old_image:any;old_location:any;old_url:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService, private activeRoute: ActivatedRoute)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addBannerImageSubmit = this.base_url_node+"admin/addorupdatebannerimage";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");


    this.id = this.activeRoute.snapshot.params['id'];
    console.log(" this.id"+ this.id);

    
    this._http.get(this.base_url_node_only+'api/admin/getSinglebannerimage/'+this.id).subscribe(res=>{
      console.log(res);
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
         this.record = this.apiResponse.data;
         this.id = this.record._id;
        this.old_title = this.record.title;
        this.old_image = this.record.image;
        this.old_location = this.record.location;
        this.old_url = this.record.url;
      }
    });
    
  }


  ngOnInit(): void {
      this.myFiles = [];this.video = null;
  }

  form = new UntypedFormGroup({
    location: new UntypedFormControl('',  [Validators.required]),
    images: new UntypedFormControl('',  [ ]),
    url: new UntypedFormControl('',  []),
    title: new UntypedFormControl('',  []),
    id: new UntypedFormControl('',  []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }


  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("image", this.myFiles[i]);
        }
         
        
      this.formData.append('location', this.form.value.location);
      this.formData.append('url', this.form.value.url);
      this.formData.append('title', this.form.value.title);
      this.formData.append('id', this.form.value.id);
      
      console.log(this.formData);
      this._http.post(this.addBannerImageSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.status == true)
          {
            let urlll = this.base_url+"allBannerMiddle";
            //window.location.href = urlll;
          }
      });
    }else{
      console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
