import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllActiveProductComponent } from './all-active-product.component';

describe('AllActiveProductComponent', () => {
  let component: AllActiveProductComponent;
  let fixture: ComponentFixture<AllActiveProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllActiveProductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllActiveProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
