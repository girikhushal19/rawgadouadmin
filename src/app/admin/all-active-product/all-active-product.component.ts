import { Component, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ModifyproductComponent } from '../modifyproduct/modifyproduct.component';
import { HttpClient } from '@angular/common/http';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options'; 
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-all-active-product',
  templateUrl: './all-active-product.component.html',
  styleUrls: ['./all-active-product.component.css']
})
export class AllActiveProductComponent implements OnInit {
  searchText: any;
  invoice_id:any[] = [];
  page_no:number=0;numbers:any;selectedIndex: number=0;
  select_all = false;allCategory:any; allSeller:any;
  base_url: any;base_url_two:any;
  ordersarray: any;apiResponse:any;
  orders: any;
  constructor(public dialog: MatDialog, private http: HttpClient, private loginAuthObj: LoginauthenticationService) {
    this.base_url = loginAuthObj.baseapiurl;
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.ordersarray = []
    this.orders = [];
    let queryParam1 = {pageno:null,is_active:"true"};
    this.http.get(this.base_url_two+"api/product/getallcatagoriesFilter").subscribe(res => {
      this.apiResponse = res;
      //console.log("this.apiResponse  --->>>>   "+JSON.stringify(this.apiResponse));
      this.allCategory = this.apiResponse.data;
    });
    this.http.get(this.base_url_two+"api/product/getSellerNameForFilter").subscribe(res => {
      this.apiResponse = res;
      //console.log("this.apiResponse  --->>>>   "+JSON.stringify(this.apiResponse));
      this.allSeller = this.apiResponse.data;
      //console.log("this.allSeller  --->>>>   "+JSON.stringify(this.allSeller));
      this.apiResponse = {};
    });

    this.http.post(this.base_url+"getallproductsforadmin", queryParam1).subscribe(res => {
      this.ordersarray = res
      //this.orders = this.ordersarray.data
      //console.log(this.orders)
      this.page_no = this.ordersarray.pages;
      this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
    });
    this.getallColor(0)
  }


  ngOnInit(): void {}
  getallColor(pageno:number=0)
  {
    this.selectedIndex = pageno;
    let queryParam = {pageno:pageno,is_active:"true"};
    this.http.post(this.base_url+"getallproductsforadmin", queryParam).subscribe(res => {
      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(this.orders)
    })
  }

  
  // data: any[] = []

  // onSelectAll(e: any): void {
  //   for (let i = 0; i < this.data.length; i++) {
  //     const item = this.data[i];
  //     item.is_selected = e;
  //   }
  // }


     
  data: any[] = [
  ]

  onSelectAll(e: any): void { 
    //console.log("is checked "+e);
    for (let i = 0; i < this.orders.length; i++)
     {
       const item = this.orders[i];
       item.is_checked = e;
       
     }
 
    if(e == true)
    {
     for (let i = 0; i < this.orders.length; i++)
     {
       const item = this.orders[i];
       item.is_checked = e;
       //console.log(item._id);
       let cc = item._id;
       this.invoice_id.push(cc);
     }
    }else{
     this.invoice_id = [];
    }
    console.log("invoiceeee "+this.invoice_id);
   }
   onSelectAll2(id:any,event:any)
   {
     console.log("id"+id);
     console.log("event"+event);
     if(event == true)
     {
       this.invoice_id.push(id);
     }else{
       const index = this.invoice_id.indexOf(id);
       this.invoice_id.splice(index, 1)
     }
 
     console.log("invoiceeee single select"+this.invoice_id);
   }


  openDialog(): void {
    const dialogRef = this.dialog.open(ModifyproductComponent, {
      height: '400px',
      width: '700px'
    });
  }




  form = new UntypedFormGroup({
    action_delete: new UntypedFormControl('', [])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
   {
      console.log(this.form.value);
      if(this.form.value.action_delete == "approve")
      {
        //console.log("approve");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url_two+"api/product/makeProductasactive/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }else if(this.form.value.action_delete == "cancel")
      {
        console.log("cancel");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            //console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url_two+"api/product/makeProductasInactive/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }else if(this.form.value.action_delete == "delete")
      {
        console.log("delete");
        //console.log("invoiceeee single select"+this.invoice_id);
        if(this.invoice_id.length > 0)
        {
          var x=0;
          var less_val = this.invoice_id.length - 1;
          for(let i=0; i<this.invoice_id.length; i++)
          {
            //console.log("invoiceeee single select"+this.invoice_id[i]);
            this.http.get(this.base_url_two+"api/product/deleteproduct/"+this.invoice_id[i]).subscribe(res => {
              this.apiResponse = res;
              if(less_val == x)
              {
                if(this.apiResponse.status == true)
                {
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }
              }

              x++;
            });
          }
        }
      }


    }

    formSecondStep = new UntypedFormGroup({
      product_name: new UntypedFormControl('', []),
      catagory: new UntypedFormControl('', []),
      is_active: new UntypedFormControl('', []),
      vendor_id: new UntypedFormControl('', []),
    });
  
    get formSecondStepFun(){
      return this.formSecondStep.controls;
    }
  
    submit2()
    {
      console.log(this.formSecondStep.value);
      let queryParam = { "catagory":this.formSecondStep.value.catagory,"is_active":this.formSecondStep.value.is_active,pageno:0,vendor_id:this.formSecondStep.value.vendor_id,product_name:this.formSecondStep.value.product_name }; 
      this.http.post(this.base_url+"getallproductsforadmin", queryParam).subscribe(res => {
        this.ordersarray = res
        this.orders = this.ordersarray.data;

      this.page_no = this.ordersarray.pages;
      this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);

        console.log(this.orders)
      })
    }


}
