import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-organizer',
  templateUrl: './edit-organizer.component.html',
  styleUrls: ['./edit-organizer.component.css']
})
export class EditOrganizerComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addDriverSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;

  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  address: string = '';
  startLatitude: number = 0;
  startLongitude: number = 0;
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;addrObj:any; allCat:any;edit_id: string = '';
  old_category: string = '';old_name: string = '';old_email: string = '';old_phone: string = '';old_address: any;old_bank: string = '';old_accountname: string = '';old_iban: string = '';old_bic: string = '';old_latlong:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService, private router: Router)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.baseapiurl2;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addDriverSubmit = this.base_url_node+"api/event/createeditordeleteorganizer";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this._http.get(this.base_url_node+"api/event/getallecatagory",{}).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        this.allCat = this.apiResponse.data;

        this.apiResponse = {};
      }
    });
    this.edit_id = this.actRoute.snapshot.params['id'];
    this._http.get(this.base_url_node+"api/event/getorganizerbyid/"+this.edit_id).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        this.allCarCategory = this.apiResponse.data;
        console.log("response of api allCarCategory"+JSON.stringify(this.allCarCategory));
        if(this.allCarCategory.length > 0)
        {
          this.old_category = this.allCarCategory[0].catagory;
          console.log("this.old_category "+this.old_category);
          this.old_name = this.allCarCategory[0].name;
          this.old_email = this.allCarCategory[0].email;
          this.old_phone = this.allCarCategory[0].phone;
          this.old_address = this.allCarCategory[0].address;
          this.old_bank = this.allCarCategory[0].bank;
          this.old_accountname = this.allCarCategory[0].accountname;
          this.old_iban = this.allCarCategory[0].iban;
          this.old_bic = this.allCarCategory[0].bic;

          console.log("this.old_address "+ JSON.stringify(this.old_address));
          this.address =  this.old_address.address;
          console.log("this.old_address "+ JSON.stringify(this.old_address));
          this.old_latlong =  this.old_address.latlong;
          if(this.old_latlong)
          {
            if(this.old_latlong.length > 0)
            {
              //console.log(" this.old_latlong "+this.old_latlong[0]);
              //console.log(" this.old_latlong "+this.old_latlong[1]);
              this.startLatitude = this.old_latlong[0];
              this.startLongitude = this.old_latlong[1];
              console.log(" this.startLatitude "+this.startLatitude);
              console.log(" this.startLongitude "+this.startLongitude);
            }
          }
          //this.old_category2 = this.allCarCategory[0].name;
        }
        this.apiResponse = {};
      }
    });

  }
  ngOnInit(): void {
      this.myFiles = []; 
      // console.log(this.ddDate);
      // console.log(this.ddDate.getHours());
      // console.log(this.ddDate.getMinutes());
      // console.log(this.ddDate.getSeconds());
  }

  handleAddressChange(address: any)
  {
    //console.log(address);
    this.address = address.formatted_address
    this.startLatitude = address.geometry.location.lat()
    this.startLongitude = address.geometry.location.lng()
  }

  form = new UntypedFormGroup({
    catagory: new UntypedFormControl('', [Validators.required]),
    fullName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required]),
    phone: new UntypedFormControl('', [Validators.required]),
    address: new UntypedFormControl('', [Validators.required]),
    accountname: new UntypedFormControl('', [Validators.required]),
    bank: new UntypedFormControl('', [Validators.required]),
    iban: new UntypedFormControl('', [Validators.required]),
    bic: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      
      this.addrObj = {
          "address":this.address,
          "latlong":[this.startLatitude,this.startLongitude]
        
      };
      this.allFormValue = {
        "op":"u",
        "catagory":this.form.value.catagory,
        "name":this.form.value.fullName,
        "email":this.form.value.email,
        "phone":this.form.value.phone,
        "address":this.addrObj,
        "accountname":this.form.value.accountname,
        "bank":this.form.value.bank,
        "iban":this.form.value.iban,
        "bic":this.form.value.bic,
      };
      console.log(this.allFormValue);
      this._http.post(this.addDriverSubmit,this.allFormValue).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == true)
        {
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            //window.location.reload();
            this.router.navigate(['allOrganizer']);
          }, 2000); 
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
