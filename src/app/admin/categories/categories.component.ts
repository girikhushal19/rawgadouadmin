import { Component, OnInit } from '@angular/core'; 
import { HttpClient } from '@angular/common/http';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  searchText:any;base_url:string='';obj:any;
  formValue:any;formData:any;
  select_all = false;apiResponse:any;allMainCategory:any;
  myFiles:string [] = [];
  constructor(private http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.baseapiurl2;
  }

  ngOnInit(): void {
    //console.log("dsfdsfd this.base_url -> "+this.base_url);
    this.http.get(this.base_url+'api/product/getallcatagoriesflat').subscribe(res=>{
      //console.log(res)
      this.apiResponse = res;
      var final_arr = {};
      if(this.apiResponse.status == true)
      {
        this.allMainCategory = this.apiResponse.data;
        
      }
    })
    this.myFiles = []; 
  }

  data: any[] = []
  
  onSelectAll(e: any): void { 
    for (let i = 0; i < this.data.length; i++) {
      const item = this.data[i];
      item.is_selected = e;
    }
  }
  form = new UntypedFormGroup({
    title: new UntypedFormControl('', [Validators.required]),
    mastercatagory: new UntypedFormControl('', []),
    images: new UntypedFormControl('', []),
  });
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit()
  {
    if(this.form.valid)
    {
      console.log(this.form.value);
      this.formData = new FormData(); 
      this.formData.append('title', this.form.value.title); 
      this.formData.append('mastercatagory', this.form.value.mastercatagory);
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("image", this.myFiles[i]);
      }
      this.http.post(this.base_url+"api/product/savecatagory",this.formData).subscribe((response:any)=>{
        console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == true)
        {
          //
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
