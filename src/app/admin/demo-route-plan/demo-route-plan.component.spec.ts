import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoRoutePlanComponent } from './demo-route-plan.component';

describe('DemoRoutePlanComponent', () => {
  let component: DemoRoutePlanComponent;
  let fixture: ComponentFixture<DemoRoutePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoRoutePlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemoRoutePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
