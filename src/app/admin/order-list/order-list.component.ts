import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  searchText:any;
  page_no:number=0;numbers:any;selectedIndex: number=0;
  base_url:any;
  apiResponse:any;base_url_two:string="";
  record:any;deletemultipleseller:string="";
  invoice_id:any[] = [];id:any;
  constructor(private loginAuthObj:LoginauthenticationService,private http: HttpClient,private actRoute: ActivatedRoute) {
    this.base_url = loginAuthObj.baseapiurl
    this.record = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    
    this.id = this.actRoute.snapshot.params['id'];

    let queryParam1 = {pageno:null};
    this.http.post(this.base_url+"alllogs",queryParam1).subscribe(res => {
      this.apiResponse = res
      this.page_no =this.apiResponse.pages;
      console.log("record "+this.record);
      this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
        console.log("this.numbers ---->  "+this.numbers);
    })

    // this.http.post(this.base_url+"alllogs",queryParam1).subscribe(res => {
    //   this.apiResponse = res
    //   this.record =this.apiResponse.data;
    //   console.log(this.record);
    // })
    this.getallColor(0);
   }
   getallColor(pageno:number=0)
  {
    this.selectedIndex = pageno;
    let queryParam = {pageno:pageno};
      this.http.post(this.base_url+"alllogs",queryParam).subscribe(res => {
        this.apiResponse = res
        this.record =this.apiResponse.data;
          console.log("record ---->  "+this.record);
      })
    
  }
  ngOnInit(): void {
  }


  form = new UntypedFormGroup({
    form_date: new UntypedFormControl('', []),
    to_date: new UntypedFormControl('', []),
    order_id: new UntypedFormControl('', []),
    status: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    if(this.form.valid)
    {
      console.log(this.form.value);
      //this.Renderchart(this.form.value.form_date,this.form.value.to_date);
      // "order_id":"",
      // "status":""
      let queryParam = {"start_date":this.form.value.form_date,"end_date":this.form.value.to_date,"order_id":this.form.value.order_id,"status":this.form.value.status,pageno:0};
      this.http.post(this.base_url+"alllogs",queryParam).subscribe(res => {
        this.apiResponse = res;
        this.record =this.apiResponse.data;
        console.log(this.record);
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}
