import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';



@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css']
})
export class AdminSettingsComponent implements OnInit {
  token:any;formData:any;base_url:any;base_url_node:any;user_type:any;apiResponse:any;formValue:any;settingSubmit:any;getModel:any;allModelList:any;getSetting:any;allCategoryType:any;
  tax_percent_old:any;max_merchant_limit_old:any;default_shipping_old:any;max_delivery_distance_old:any;driver_support_mobile_old:any;driver_support_email_old:any;max_pickup_distance_old:any;  getReturnPriceAPI=""; getReturnPrice:number=0;
  admin_commission_old:any;return_max_day_old:any; getReturnPriceData:any; base_url_node_plan:string="";old_auction_amount:number=0; old_auction_amount_percentage:number=0;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plan = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.settingSubmit = this.base_url_node+"settingSubmit";
    //this.getModel = this.base_url_node+"getModel";
    this.getSetting = this.base_url_node+"getSetting";

    this.getReturnPriceAPI = this.base_url_node_plan+"admin-panel/getglobalsettings";

    //console.log("here");
    
  }
  ngOnInit(): void {
    this._http.post(this.getSetting,{}).subscribe((response:any)=>{
      this.allCategoryType = response.result;
      console.log("response of api"+this.allCategoryType[0]);
      console.log("response of api"+this.allCategoryType[1]);
      //console.log("response of api"+JSON.stringify(this.allCategoryType));
      //this.apiResponse = response;    
      for(let i=0; i<this.allCategoryType.length; i++)
      {
        if(this.allCategoryType[i].attribute_key == "tax_percent")
        {
          this.tax_percent_old = this.allCategoryType[i].attribute_value;
        }
        if(this.allCategoryType[i].attribute_key == "max_merchant_limit")
        {
          this.max_merchant_limit_old = this.allCategoryType[i].attribute_value;
        }
        if(this.allCategoryType[i].attribute_key == "return_max_day")
        {
          this.return_max_day_old = this.allCategoryType[i].attribute_value;
        }
        if(this.allCategoryType[i].attribute_key == "auction_amount")
        {
          this.old_auction_amount = this.allCategoryType[i].attribute_value;
        }
        if(this.allCategoryType[i].attribute_key == "auction_amount_percentage")
        {
          this.old_auction_amount_percentage = this.allCategoryType[i].attribute_value;
        }

        
      }
    });

    this._http.get(this.getReturnPriceAPI,{}).subscribe((response:any)=>{
      console.log("76 no. response of api"+ JSON.stringify(response));
      if(response.status == true)
      {
        console.log("ifffffffffffffffff ");
        if( response.data)
        {
          this.getReturnPriceData = response.data;
          this.getReturnPrice = parseFloat(this.getReturnPriceData.returnprice);
          console.log("getReturnPriceData  "+ this.getReturnPrice );

          //
          
          // console.log("getReturnPrice  "+ JSON.stringify( this.getReturnPrice) );
          
          // if(isNaN(this.getReturnPrice))
          // {
          //   console.log("is nan");
          //   this.getReturnPrice = 0;
          // }
        }else{
          this.getReturnPrice = 0;
        }
      }else{
        this.getReturnPrice = 0;
      }
    });
    console.log("922222222 getReturnPrice  "+this.getReturnPrice);

  }

  form = new UntypedFormGroup({
    tax_percent: new UntypedFormControl('', [Validators.required]),
    //max_merchant_limit: new UntypedFormControl('', [Validators.required]),
    return_max_day: new UntypedFormControl('', [Validators.required]),
    returnprice: new UntypedFormControl('', [Validators.required]),
    auction_amount: new UntypedFormControl('', [Validators.required]),
    auction_amount_percentage: new UntypedFormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {

      this._http.post(this.base_url_node_plan+"admin-panel/updateoraddSettings",this.form.value).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          // setTimeout(() => {
          //   window.location.reload();
          // }, 2000); 
        }
      });


      this._http.post(this.settingSubmit,this.form.value).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
            window.location.reload();
          }, 2000); 
        }
      });
      
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
