import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private adminLogin:string = "";
  formValue:any;base_url:any;
  apiResponse:any;token:any;user_type:any;
  apiStringify:any;user_id:any;loggedInDetail:any;base_url_node_admin:any;
  base_url_node:string="";
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url_node_admin = loginAuthObj.base_url_node_admin;
    this.base_url = loginAuthObj.base_url;
    console.log(this.base_url_node_admin);
    this.adminLogin = this.base_url_node_admin+"adminLogin";    

    this.base_url_node =  loginAuthObj.base_url_node;
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    console.log("token "+this.token);
    console.log("user_type "+this.user_type);
    if(this.token !== "")
    {
      if(this.user_type === "admin" )
      {
        //console.log("here");
        window.location.href = this.base_url+"dashboard";
      }
    }
  }

  ngOnInit(): void {
  }

  form = new UntypedFormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('',[Validators.required, Validators.minLength(6)])
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit(){
    console.log("here");
    if (this.form.valid)
    {
      this._http.post(this.adminLogin,this.form.value).subscribe((response:any)=>{
        console.log(response);
        this.apiResponse = response;
        //console.log(JSON.stringify(this.apiResponse));
        if(this.apiResponse.error == false)
        {
          localStorage.setItem("token",this.apiResponse.userRecord.token);
          localStorage.setItem("_id",this.apiResponse.userRecord._id);
          localStorage.setItem("email",this.apiResponse.userRecord.email);
          localStorage.setItem("user_type",this.apiResponse.userRecord.user_type);
        
          window.location.href = this.base_url+"dashboard";
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
