import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GoogleMapsModule } from '@angular/google-maps';
import { NgxEditorModule } from 'ngx-editor';
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';

import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminheaderComponent } from './adminheader/adminheader.component';
import { AdminfooterComponent } from './adminfooter/adminfooter.component';
import { AdminsidebarComponent } from './adminsidebar/adminsidebar.component';
import { LogoutComponent } from './logout/logout.component';
import { DemoRoutePlanComponent } from './demo-route-plan/demo-route-plan.component';
import { DemoRoutePlanNewComponent } from './demo-route-plan-new/demo-route-plan-new.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { DemoTimePickerComponent } from './demo-time-picker/demo-time-picker.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AllDriversComponent } from './all-drivers/all-drivers.component';
import { EditDriversComponent } from './edit-drivers/edit-drivers.component';
import { AddRouteComponent } from './add-route/add-route.component';
import { AllRouteComponent } from './all-route/all-route.component';
import { EditRouteComponent } from './edit-route/edit-route.component';
import { AssignRouteComponent } from './assign-route/assign-route.component';
import { AssignRouteToDriverComponent } from './assign-route-to-driver/assign-route-to-driver.component';
import { EditStopComponent } from './edit-stop/edit-stop.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { UsersComponent } from './users/users.component';
import { VendormanagementComponent } from './vendormanagement/vendormanagement.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { SellersComponent } from './sellers/sellers.component';
import { AbusereportComponent } from './abusereport/abusereport.component';
import { StorereviewsComponent } from './storereviews/storereviews.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { RefundsComponent } from './refunds/refunds.component';
import { ReportsComponent } from './reports/reports.component';
import { AllproductsComponent } from './allproducts/allproducts.component';
import { CategoriesComponent } from './categories/categories.component';
import { OrdersComponent } from './orders/orders.component';
import { AddplanComponent } from './addplan/addplan.component';
import { StatisticsOComponent } from './statistics-o/statistics-o.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgChartsModule } from 'ng2-charts';
import {MatDialogModule} from '@angular/material/dialog';
import { AddadvertiseComponent } from './addadvertise/addadvertise.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { OrderreportComponent } from './orderreport/orderreport.component';
import { ShipingareaComponent } from './shipingarea/shipingarea.component';
import { AddshiipingareaComponent } from './addshiipingarea/addshiipingarea.component';
import { ModifyproductComponent } from './modifyproduct/modifyproduct.component';
import { EditvendorComponent } from './editvendor/editvendor.component';
import { MarkerplaceComponent } from './markerplace/markerplace.component';
import { AdmincategoryComponent } from './admincategory/admincategory.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { ShippingLabelComponent } from './shipping-label/shipping-label.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {PrintService} from './print.service';
import { AllMerchantComponent } from './all-merchant/all-merchant.component';
import { AllShipmentComponent } from './all-shipment/all-shipment.component';
import { AllClientComponent } from './all-client/all-client.component';
import { AdminClientDetailComponent } from './admin-client-detail/admin-client-detail.component';
import { AdminShipmentDetailComponent } from './admin-shipment-detail/admin-shipment-detail.component';
import { EditDriverSupportComponent } from './edit-driver-support/edit-driver-support.component';
import { AdminSettingsComponent } from './admin-settings/admin-settings.component';
import { SendPushNotificationMerchantComponent } from './send-push-notification-merchant/send-push-notification-merchant.component';
import { AllPushNotificationComponent } from './all-push-notification/all-push-notification.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { AllInvoiceComponent } from './all-invoice/all-invoice.component';
import { AllInvoiceShipmentComponent } from './all-invoice-shipment/all-invoice-shipment.component';
import { AllCategoryComponent } from './all-category/all-category.component';
import { AllMarketPlaceComponent } from './all-market-place/all-market-place.component';
import { GetVendorBanksComponent } from './get-vendor-banks/get-vendor-banks.component';
import { EditBankDetailComponent } from './edit-bank-detail/edit-bank-detail.component';
import { OrderListComponent } from './order-list/order-list.component';
import { AddOrganizerComponent } from './add-organizer/add-organizer.component';
import { AllOrganizerComponent } from './all-organizer/all-organizer.component';
import { EditOrganizerComponent } from './edit-organizer/edit-organizer.component';
import { AddEventCategoryComponent } from './add-event-category/add-event-category.component';
import { AllEventCategoryComponent } from './all-event-category/all-event-category.component';
import { EditEventCategoryComponent } from './edit-event-category/edit-event-category.component';
import { AddEventComponent } from './add-event/add-event.component';
import { AllEventComponent } from './all-event/all-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { SellerTermsConditionComponent } from './seller-terms-condition/seller-terms-condition.component';
import { EditMarcketPlaceComponent } from './edit-marcket-place/edit-marcket-place.component';
import { EditZoneNameComponent } from './edit-zone-name/edit-zone-name.component';
import { EditPlanComponent } from './edit-plan/edit-plan.component';
import { EditWebPagesComponent } from './edit-web-pages/edit-web-pages.component';
import { AddBannerMiddleComponent } from './add-banner-middle/add-banner-middle.component';
import { AllBannerMiddleComponent } from './all-banner-middle/all-banner-middle.component';
import { EditBannerMiddleComponent } from './edit-banner-middle/edit-banner-middle.component';
import { AddEventBannerComponent } from './add-event-banner/add-event-banner.component';
import { AllEventBannerComponent } from './all-event-banner/all-event-banner.component';
import { EditEventBannerComponent } from './edit-event-banner/edit-event-banner.component';
import { AllExpiredEventComponent } from './all-expired-event/all-expired-event.component';
import { AllOngoingEventComponent } from './all-ongoing-event/all-ongoing-event.component';
import { AllOrganizerEarningComponent } from './all-organizer-earning/all-organizer-earning.component';
import { MakePaymentRequestComponent } from './make-payment-request/make-payment-request.component';
import { OrganiserPaymentHistoryComponent } from './organiser-payment-history/organiser-payment-history.component';
import { ChangeMerchantCreditComponent } from './change-merchant-credit/change-merchant-credit.component';
import { GetVendorPaymentRequestComponent } from './get-vendor-payment-request/get-vendor-payment-request.component';
import { AllEventBookingComponent } from './all-event-booking/all-event-booking.component';
import { ActiveSellerComponent } from './active-seller/active-seller.component';
import { InActiveSellerComponent } from './in-active-seller/in-active-seller.component';
import { DeletedSellerComponent } from './deleted-seller/deleted-seller.component';
import { AllActiveUsersComponent } from './all-active-users/all-active-users.component';
import { AllInActiveUsersComponent } from './all-in-active-users/all-in-active-users.component';
import { AllDeletedUsersComponent } from './all-deleted-users/all-deleted-users.component';
import { AllStopComponent } from './all-stop/all-stop.component';
import { AllInactiveProductComponent } from './all-inactive-product/all-inactive-product.component';
import { AllActiveProductComponent } from './all-active-product/all-active-product.component';
import { AllDeletedProductComponent } from './all-deleted-product/all-deleted-product.component';
import { AllLattestOrderComponent } from './all-lattest-order/all-lattest-order.component';
import { AllUserCancledOrderComponent } from './all-user-cancled-order/all-user-cancled-order.component';
import { AllCompletedOrderComponent } from './all-completed-order/all-completed-order.component';
import { AllRadyToPickUpOrderComponent } from './all-rady-to-pick-up-order/all-rady-to-pick-up-order.component';
import { AllPickedUpOrderComponent } from './all-picked-up-order/all-picked-up-order.component';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';
//import { DataTablesModule } from "angular-datatables";

@NgModule({
  declarations: [
    LoginComponent,
    DashboardComponent,
    AdminheaderComponent,
    AdminfooterComponent,
    AdminsidebarComponent,
    LogoutComponent,
    DemoRoutePlanComponent,
    DemoRoutePlanNewComponent,
    AddDriverComponent,
    DemoTimePickerComponent,
    AllDriversComponent,
    EditDriversComponent,
    AddRouteComponent,
    AllRouteComponent,
    EditRouteComponent,
    AssignRouteComponent,
    AssignRouteToDriverComponent,
    EditStopComponent,
    StatisticsComponent,
    UsersComponent,
    VendormanagementComponent,
    WithdrawComponent,
    SellersComponent,
    AbusereportComponent,
    StorereviewsComponent,
    AdvertisementComponent,
    RefundsComponent,
    ReportsComponent,
    AllproductsComponent,
    CategoriesComponent,
    OrdersComponent,
    AddplanComponent,
    StatisticsOComponent,
    AddadvertiseComponent,
    EditprofileComponent,
    OrderreportComponent,
    ShipingareaComponent,
    AddshiipingareaComponent,
    ModifyproductComponent,
    EditvendorComponent,
    MarkerplaceComponent,
    AdmincategoryComponent,
    ShippingLabelComponent,
    PrintLayoutComponent,
    AllMerchantComponent,
    AllShipmentComponent,
    AllClientComponent,
    AdminClientDetailComponent,
    AdminShipmentDetailComponent,
    EditDriverSupportComponent,
    AdminSettingsComponent,
    SendPushNotificationMerchantComponent,
    AllPushNotificationComponent,
    AddInvoiceComponent,
    AllInvoiceComponent,
    AllInvoiceShipmentComponent,
    AllCategoryComponent,
    AllMarketPlaceComponent,
    GetVendorBanksComponent,
    EditBankDetailComponent,
    OrderListComponent,
    AddOrganizerComponent,
    AllOrganizerComponent,
    EditOrganizerComponent,
    AddEventCategoryComponent,
    AllEventCategoryComponent,
    EditEventCategoryComponent,
    AddEventComponent,
    AllEventComponent,
    EditEventComponent,
    OrderDetailComponent,
    EditCategoryComponent,
    SellerTermsConditionComponent,
    EditMarcketPlaceComponent,
    EditZoneNameComponent,
    EditPlanComponent,
    EditWebPagesComponent,
    AddBannerMiddleComponent,
    AllBannerMiddleComponent,
    EditBannerMiddleComponent,
    AddEventBannerComponent,
    AllEventBannerComponent,
    EditEventBannerComponent,
    AllExpiredEventComponent,
    AllOngoingEventComponent,
    AllOrganizerEarningComponent,
    MakePaymentRequestComponent,
    OrganiserPaymentHistoryComponent,
    ChangeMerchantCreditComponent,
    GetVendorPaymentRequestComponent,
    AllEventBookingComponent,
    ActiveSellerComponent,
    InActiveSellerComponent,
    DeletedSellerComponent,
    AllActiveUsersComponent,
    AllInActiveUsersComponent,
    AllDeletedUsersComponent,
    AllStopComponent,
    AllInactiveProductComponent,
    AllActiveProductComponent,
    AllDeletedProductComponent,
    AllLattestOrderComponent,
    AllUserCancledOrderComponent,
    AllCompletedOrderComponent,
    AllRadyToPickUpOrderComponent,
    AllPickedUpOrderComponent,
    EditAdminProfileComponent
  ],
  
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    GoogleMapsModule,
    GooglePlaceModule,
    Ng2SearchPipeModule,
    NgChartsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    NgxEditorModule,
    AngularEditorModule,
    NgxPaginationModule,
    NgxBarcodeModule,
    NgMultiSelectDropDownModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger', // set defaults here
    }),
    
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    
  ],
  providers: [PrintService],
})
export class AdminModule { }
