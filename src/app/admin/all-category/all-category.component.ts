import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-all-category',
  templateUrl: './all-category.component.html',
  styleUrls: ['./all-category.component.css']
})
export class AllCategoryComponent implements OnInit {
  searchText:any;
  base_url:any; 
  vendorsarray:any;base_url_two:string="";apiResponse:any;apiResponse2:any;
  vendors:any;deletemultipleseller:string="";
  invoice_id:any[] = [];
  totalPageNumber:any;numbers:any;selectedIndex: number;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService,private http: HttpClient) {
    this.base_url = loginAuthObj.baseapiurl
    this.vendorsarray =[]
    this.vendors = []
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.deletemultipleseller = this.base_url_two+"api/seller/deletemultipleseller";

    
    this.selectedIndex = 0;

    this.http.get(this.base_url_two+"api/product/getAllCatagoriesCount").subscribe(res => {
      this.apiResponse2 = res
      console.log(this.apiResponse2);
      this.totalPageNumber = this.apiResponse2.data;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      console.log("this.numbers "+this.numbers);
    })
    this.getallBanner(0);
   }

  ngOnInit(): void {
    

  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    
    this.http.post(this.base_url_two+"api/product/getAllCatagoriesPagi",{"pageno":numofpage}).subscribe(res => {
      this.apiResponse2 = res
       this.vendors = this.apiResponse2.data;
      
    })
  }
  updateUserStatus(id=null,status:any)
  {
    /*console.log("hii");
    console.log(id);*/
    let queryParam = {"id":id,"status":status};
    this._http.post(this.base_url_two+"api/product/updateCatStatus",queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.status == true)
      {
        setTimeout(()=>{
          window.location.reload();
        },1000);
      }
    });
  }

  makeFeature(id:any,status:any)
  {
    console.log("id ",id);
    console.log("status ",status);
    
    this.http.post(this.base_url_two+"api/product/makeFeatureApi",{"id":id,status:status}).subscribe(res => {
      this.apiResponse = res;
       if( this.apiResponse.status ==true )
       {
        window.location.reload();
       }
    })

  }
  select_all = false;
  data: any[] = [
  ]

  onSelectAll(e: any): void { 
    //console.log("is checked "+e);
    for (let i = 0; i < this.vendors.length; i++)
     {
       const item = this.vendors[i];
       item.is_checked = e;
       
     }
 
    if(e == true)
    {
     for (let i = 0; i < this.vendors.length; i++)
     {
       const item = this.vendors[i];
       item.is_checked = e;
       //console.log(item._id);
       let cc = item._id;
       this.invoice_id.push(cc);
     }
    }else{
     this.invoice_id = [];
    }
    console.log("invoiceeee "+this.invoice_id);
   }
   onSelectAll2(id:any,event:any)
   {
     console.log("id"+id);
     console.log("event"+event);
     if(event == true)
     {
       this.invoice_id.push(id);
     }else{
       const index = this.invoice_id.indexOf(id);
       this.invoice_id.splice(index, 1)
     }
 
     console.log("invoiceeee single select"+this.invoice_id);
   }
  



}
