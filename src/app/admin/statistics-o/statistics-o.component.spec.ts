import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsOComponent } from './statistics-o.component';

describe('StatisticsOComponent', () => {
  let component: StatisticsOComponent;
  let fixture: ComponentFixture<StatisticsOComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatisticsOComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StatisticsOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
