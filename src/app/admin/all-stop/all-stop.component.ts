import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-all-stop',
  templateUrl: './all-stop.component.html',
  styleUrls: ['./all-stop.component.css']
})
export class AllStopComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allRoutePlan:any;queryParam:any;numbers:any;allRoutePlanCount:any;apiStringify:any;updateRoutePlanStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;
  selectedIndex: number;deleteRoutePlan:any; edit_id:string='';
  //dtOptions: DataTables.Settings = {}; 
  getqueryParam:any;assignToDriverApi:string='';
  record2:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    //console.log(this.base_url_node);


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    //this.exportCsvDriverApi = this.base_url_node+"exportCsvDriverApi";
    this.allRoutePlan = this.base_url_node+"allRouteStop";
    
    this.selectedIndex = 0;

    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log("this.edit_id ",this.edit_id);
    this._http.get(this.base_url_node+"allRouteStop/"+this.edit_id).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log("response  -->>> "+ JSON.stringify(response));
      if(this.apiResponse.status == true)
      {
        //console.log("response  -->>> "+ JSON.stringify(response));
        this.record = this.apiResponse.route_record;
        this.record2 =  this.apiResponse.stop_record;

        // console.log("record  -->>> "+ JSON.stringify(this.record));
        // console.log("record2  -->>> "+ JSON.stringify(this.record2));

      }
      
      //console.log("record  -->>> "+ JSON.stringify(this.record2));
      //console.log("deliveryaddress new  -->>> "+ this.record[0].sellerinfo[0].deliveryaddress );
    });

    
  }

  ngOnInit(): void {

  }
  

  // exportCsv()
  // {
  //   this.queryParam = {"pageNumber":0};
  //   this._http.get(this.exportCsvDriverApi,this.queryParam).subscribe((response:any)=>{ 
  //     console.log(response);
  //     this.apiResponse = response;
  //     if(this.apiResponse.error == false)
  //     {
         
  //     }
  //   });
  // }

 

}
