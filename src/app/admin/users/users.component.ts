import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { EditprofileComponent } from '../editprofile/editprofile.component';
import { HttpClient } from '@angular/common/http';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  searchText:any;usersarray:any;users:any;orders:any;base_url:any;deletemultipleuser:string="";apiResponse:any;
  page_no:number=0;numbers:any;selectedIndex: number=0;
  invoice_id:any[] = [];base_url_two:string="";totalCounnt:number=0;
  constructor(private _http:HttpClient,public dialog: MatDialog,private http: HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.users =[]
    this.usersarray = []
    this.orders = []
    this.base_url = loginAuthObj.baseapiurl
    this.base_url_two = loginAuthObj.baseapiurl2;
    this.deletemultipleuser = this.base_url_two+"api/user/deletemultipleuser";

    this.http.get(this.base_url+"users/null").subscribe(res => {
      this.usersarray = res;

      if(this.usersarray.status == true)
      {
        //console.log("this.usersarray "+JSON.stringify(this.usersarray));
        this.page_no =this.usersarray.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
        //console.log("this.numbers ---->  "+this.numbers);
        this.totalCounnt = this.usersarray.totalCounnt;
      }
      

    })
    
    this.getallColor(0);
  }
  
  ngOnInit(): void {
    
  }
  form2 = new UntypedFormGroup({
    firstName: new UntypedFormControl('', []),
    lastName: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', []), 
    status: new UntypedFormControl('', []),
    //selectedIndex: new UntypedFormControl(this.selectedIndex, []),
    
  });
  submit2()
  {
      this.http.post(this.base_url+"usersFilter",this.form2.value).subscribe(res => {
        this.usersarray = res;
        
        this.users = this.usersarray.data;
        //console.log("this.users "+JSON.stringify(this.users));
        this.page_no =this.usersarray.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
      })
  }

  select_all = false;
  data: any[] = []
  getallColor(pageno:number=0)
  {
      this.selectedIndex = pageno;
      this.http.get(this.base_url+"users/"+pageno).subscribe(res => {
        this.usersarray = res;
        
        this.users = this.usersarray.data;
        //console.log("this.users "+JSON.stringify(this.users));
      })

  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(EditprofileComponent ,{
      height: '400px',
      width: '800px'
    });
  }

  onSelectAll(e: any): void { 
   //console.log("is checked "+e);
   for (let i = 0; i < this.users.length; i++)
    {
      const item = this.users[i];
      item.is_checked = e;
      
    }

   if(e == true)
   {
    for (let i = 0; i < this.users.length; i++)
    {
      const item = this.users[i];
      item.is_checked = e;
      //console.log(item._id);
      let cc = item._id;
      this.invoice_id.push(cc);
    }
   }else{
    this.invoice_id = [];
   }
   //console.log("invoiceeee "+this.invoice_id);
  }
  onSelectAll2(id:any,event:any)
  {
    // console.log("id"+id);
    // console.log("event"+event);
    if(event == true)
    {
      this.invoice_id.push(id);
    }else{
      const index = this.invoice_id.indexOf(id);
      this.invoice_id.splice(index, 1)
    }

    //console.log("invoiceeee single select"+this.invoice_id);
  }

  deleteUser(id:any){
    console.log(id)
      if(confirm("Voulez-vous supprimer cet utilisateur ?") == true){
        this.http.get(this.base_url+"deleteuser/"+id).subscribe(res => {
          //alert("user deleted successfully")
          //console.log(res)
          this.apiResponse= res;
          if(this.apiResponse.status == true)
          {
            setTimeout(() => {
              window.location.reload();
            }, 2000); 
          }

        })
      }
  }
  form = new UntypedFormGroup({
    action_delete: new UntypedFormControl('', [])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    //console.log(this.form.value);
    if(this.form.value.action_delete == "delete")
    {
      let queryParam = {"ids":this.invoice_id};
      this._http.post(this.deletemultipleuser,queryParam).subscribe((response:any)=>{
        //console.log("response"+response);
        this.apiResponse= response;
        if(this.apiResponse.status == true)
        {
          setTimeout(() => {
            window.location.reload();
          }, 1000); 
        }
      });
    }
  }
  inactiveUser(id:any)
  {
    console.log(id);
    //{{local}}/admin-panel//63bd435dee7014ce11fe2cf3
    this.http.get(this.base_url+"markuserasinactive/"+id).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      // console.log(this.apiResponse)
      // console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 2000); 
      }
      //console.log(res)
    });
  }
  activeUser(id:any)
  {
    //console.log(id);
    this.http.get(this.base_url+"markuserasactive/"+id).subscribe(res => {
      this.apiResponse = res;
      //alert("user deleted successfully")
      // console.log(this.apiResponse)
      // console.log("status "+this.apiResponse.status)
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          window.location.reload();
        }, 2000); 
      }
    });
  }
}
