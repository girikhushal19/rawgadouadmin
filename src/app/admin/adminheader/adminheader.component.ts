import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common'; 

@Component({
  selector: 'app-adminheader',
  templateUrl: './adminheader.component.html',
  styleUrls: ['./adminheader.component.css']
})
export class AdminheaderComponent implements OnInit {

  displayStyle:string = "none";
  newMessageCount:number = 0;
  base_url = "";base_url_node = ""; attributeArrayValue:any;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editRestaurantSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getMerchantNotification:any;getqueryParam:any;
  base_url_node_only:any;record:any; user_id:any;


  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    this.user_id = this.loginAuthObj.userLoggedIn();
    
    console.log("user_id "+this.user_id);
    this.getqueryParam = {"user_id":this.user_id};
    this.getMerchantNotification = this.base_url_node+"common/getAdminNotification/"+this.user_id; 

    setInterval(()=>{},1000)
      
    
      this._http.get(this.getMerchantNotification ).subscribe((response:any)=>{
        //console.log("this record "+JSON.stringify(response));
        
        if(response.status == true)
        {
          this.newMessageCount = response.newMessageCount;
          this.record = response.data;
        }
         //console.log("this newMessageCount "+this.newMessageCount);
       //console.log("this record "+JSON.stringify(this.record));
      });
    
    
  }

  ngOnInit(): void {
  }
  openNotificatioModel()
  {
    if(this.displayStyle == "none")
    {
      this.displayStyle = "block";
    }else{
      this.displayStyle = "none";
    }
  }

}
