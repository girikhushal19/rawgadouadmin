import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-merchant',
  templateUrl: './all-merchant.component.html',
  styleUrls: ['./all-merchant.component.css']
})
export class AllMerchantComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allMerchant:any;queryParam:any;numbers:any;allMerchantCount:any;apiStringify:any;updateMerchantStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;
  selectedIndex: number;deleteDriverACApi:any;
  //dtOptions: DataTables.Settings = {}; 


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    //console.log(this.base_url_node);


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      // window.location.href = this.base_url;
    }

    this.exportCsvDriverApi = this.base_url_node+"exportCsvDriverApi";
    this.allMerchantCount = this.base_url_node+"allMerchantCount";
    this.allMerchant = this.base_url_node+"allMerchant";
    this.updateMerchantStatusApi = this.base_url_node+"updateMerchantStatusApi";
    this.deleteDriverACApi = this.base_url_node+"deleteDriverACApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {


    // this.dtOptions = {
    //   "searching": false,
    //   "responsive": true,
    //   "autoWidth": false,
    //   "language": {
	  //   "search": "Recherche",
	  //   //"info": "Recherche",
	  //   //"entries": "Recherche",
	  //   }
    // };
    this._http.post(this.allMerchantCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allMerchantCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allMerchant,this.queryParam).subscribe((response:any)=>{
      //console.log("allMerchant"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateUserStatus(id=null)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id,"status":true};
    this._http.post(this.updateMerchantStatusApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }

  updateUserStatusInactive(id=null)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id,"status":false};
    this._http.post(this.updateMerchantStatusApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }

  deleteDriverAC(id=null)
  {
    this.queryParam = {"id":id};
    this._http.post(this.deleteDriverACApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  exportCsv()
  {
    this.queryParam = {"pageNumber":0};
    this._http.get(this.exportCsvDriverApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
         
      }
    });
  }


  form = new UntypedFormGroup({
    first_name: new UntypedFormControl('', []),
    last_name: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    phone: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []), 
    status: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    console.log(this.form.value);
    this._http.post(this.allMerchant,this.form.value).subscribe((response:any)=>{
      //console.log("allMerchant"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }


}
