import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-assign-route',
  templateUrl: './assign-route.component.html',
  styleUrls: ['./assign-route.component.css']
})
export class AssignRouteComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allDrivers:any;queryParam:any;numbers:any;allDriversCount:any;apiStringify:any;updateDriverStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;
  selectedIndex: number;deleteDriverACApi:any;route_id:any;
  //dtOptions: DataTables.Settings = {}; 


  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    //console.log(this.base_url_node);


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.exportCsvDriverApi = this.base_url_node+"exportCsvDriverApi";
    this.allDriversCount = this.base_url_node+"allDriversForAssignCount";
    this.allDrivers = this.base_url_node+"allDriversForAssign";
    this.updateDriverStatusApi = this.base_url_node+"updateDriverStatusApi";
    this.deleteDriverACApi = this.base_url_node+"deleteDriverACApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
    this.route_id = this.actRoute.snapshot.params['id'];
  }

  ngOnInit(): void {


    // this.dtOptions = {
    //   "searching": false,
    //   "responsive": true,
    //   "autoWidth": false,
    //   "language": {
	  //   "search": "Recherche",
	  //   //"info": "Recherche",
	  //   //"entries": "Recherche",
	  //   }
    // };
    this._http.post(this.allDriversCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allDriversCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allDrivers,this.queryParam).subscribe((response:any)=>{
      //console.log("allDrivers"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateUserStatus(id=null,status:number)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateDriverStatusApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  } 


  form = new UntypedFormGroup({
    fullName: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []), 
    status: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    console.log(this.form.value);
    this._http.post(this.allDrivers,this.form.value).subscribe((response:any)=>{
      //console.log("allDrivers"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }


}
