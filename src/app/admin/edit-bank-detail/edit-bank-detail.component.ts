import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit-bank-detail',
  templateUrl: './edit-bank-detail.component.html',
  styleUrls: ['./edit-bank-detail.component.css']
})
export class EditBankDetailComponent implements OnInit {
  base_url: any;
  usersarray: any;old_userImage:string="";base_url_two:string="";
  myFiles:string [] = [];formData:any;apiResponse:any;
  users: any;id:any;bank_id:any;seller_email:any;
  form = new UntypedFormGroup({
    email: new UntypedFormControl('', []),
    bank_id: new UntypedFormControl('', []),
    account_name: new UntypedFormControl('', [Validators.required]),
    account_number: new UntypedFormControl('', [Validators.required]),
    bank_name: new UntypedFormControl('', [Validators.required]),
    bank_address: new UntypedFormControl('', [Validators.required]),
    iban :new UntypedFormControl('', [Validators.required]),
    bic: new UntypedFormControl('', [Validators.required]),
  });
  constructor(private http: HttpClient, private activeRoute: ActivatedRoute, private loginAuthObj: LoginauthenticationService,
    private router: Router)
    {
      this.id = this.activeRoute.snapshot.params['id'];
      this.bank_id = this.activeRoute.snapshot.params['bank_id'];

      this.base_url = loginAuthObj.baseapiurl
      this.base_url_two = loginAuthObj.baseapiurl2;
      this.users = []
      this.usersarray = []
      let queryParam = {"id":this.id,"bank_id":this.bank_id};
      this.http.post(this.base_url_two + "api/seller/getbankbybankid/",queryParam).subscribe(res => {
        //console.log("api response "+res);
        this.usersarray = res
        this.users = this.usersarray.data.account;
        //console.log("this.users  "+ JSON.stringify(this.users));
        const user = this.usersarray.data.account[0];
        this.seller_email = this.usersarray.data.email;
        console.log("user "+JSON.stringify(user));
        if (user) {
          this.form = new UntypedFormGroup({
            email:new UntypedFormControl(this.seller_email),
            bank_id:new UntypedFormControl(this.bank_id),
            account_name: new UntypedFormControl(user.account_name, [Validators.required]),
            account_number: new UntypedFormControl(user.account_number, [Validators.required]),
            bank_name: new UntypedFormControl(user.bank_name, [Validators.required]),
            bank_address: new UntypedFormControl(user.bank_address, [Validators.required]),
            iban: new UntypedFormControl(user.iban, [Validators.required]),
            bic: new UntypedFormControl(user.bic, [Validators.required]),
          });
  
        }
      })

    }

  ngOnInit(): void {
  }
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit() {
    console.log(this.form.value)
    this.http.post(this.base_url + "updatevendors", this.form.value).subscribe(res => {
      console.log(res);
      //this.router.navigate(['sellers'])
      this.apiResponse = res;
      if(this.apiResponse.status == true)
      {
        setTimeout(() => {
          this.router.navigate(['sellers'])
        }, 1000); 
      }
    })
  }

}
