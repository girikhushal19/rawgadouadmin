import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';

@Component({
  selector: 'app-addplan',
  templateUrl: './addplan.component.html',
  styleUrls: ['./addplan.component.css']
})
export class AddplanComponent implements OnInit {
  searchText:any;base_url:string='';obj:any;
  formValue:any;formData:any;
  select_all = false;apiResponse:any;allMainCategory:any;
  oldType:string='a';
  constructor(private http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.baseapiurl2;
  }

  ngOnInit(): void {
    //console.log("dsfdsfd this.base_url -> "+this.base_url);
    
  }

  
  
  form = new UntypedFormGroup({
    package: new UntypedFormControl('', [Validators.required]),
    plusperproduct: new UntypedFormControl('', [Validators.required]),
    extracommission: new UntypedFormControl('', [Validators.required]),
    price: new UntypedFormControl('', [Validators.required]),
    duration: new UntypedFormControl('', [Validators.required]),
    type: new UntypedFormControl('a', []),
    Numberofitems: new UntypedFormControl('', [Validators.required]),
    is_limited: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', []), 
    
  });
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  submit()
  {
    if(this.form.valid)
    {
      console.log(this.form.value);
      this.http.post(this.base_url+"api/subs/createoreditsubpackage",this.form.value).subscribe((response:any)=>{
        console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == 'success')
        {
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
