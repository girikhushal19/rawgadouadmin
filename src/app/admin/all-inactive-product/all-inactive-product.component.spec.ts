import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllInactiveProductComponent } from './all-inactive-product.component';

describe('AllInactiveProductComponent', () => {
  let component: AllInactiveProductComponent;
  let fixture: ComponentFixture<AllInactiveProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllInactiveProductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllInactiveProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
