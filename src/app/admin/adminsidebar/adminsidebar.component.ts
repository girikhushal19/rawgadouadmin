import { Component, OnInit } from '@angular/core';
import { Router,NavigationStart, Event as NavigationEvent } from '@angular/router';
import { Location } from '@angular/common';

import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-adminsidebar',
  templateUrl: './adminsidebar.component.html',
  styleUrls: ['./adminsidebar.component.css']
})
export class AdminsidebarComponent implements OnInit {
  currentRoute:any;currentUrlSegment:any;urlLength:any;
  logged_in_user_id:any;user_type:any;
  queryParam:any;userPermissionRecord:any;base_url:any;base_url_node:any;base_url_node_plain:any;
  record:any;apiResponse:any;base_url_node_only:any;
  getAdminProfile:any;
   removeCategoryImage:any;old_email:any;old_firstName:any;old_lastName:any;old_userImage:any;old_id:any;getqueryParam:any;
  constructor(private router: Router,private Location:Location,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.currentRoute = this.router.url.split('/');
      //console.log(this.currentRoute);
      //console.log(this.currentRoute.length);
      if(this.currentRoute.length == 4)
      {
        //console.log("here");
        this.urlLength = this.currentRoute.length-3;
        this.currentUrlSegment = this.currentRoute[this.urlLength];
      }else if(this.currentRoute.length == 3)
      {
        //console.log("here");
        this.urlLength = this.currentRoute.length-2;
        this.currentUrlSegment = this.currentRoute[this.urlLength];
      }else{
        this.urlLength = this.currentRoute.length-1;
        this.currentUrlSegment = this.currentRoute[this.urlLength];
      }


      this.logged_in_user_id = localStorage.getItem("_id");
      this.user_type = localStorage.getItem("user_type");
      //console.log("logged_in_user_id "+this.logged_in_user_id);
      //console.log(this.user_type);

      this.base_url = this.loginAuthObj.base_url;
      this.base_url_node = this.loginAuthObj.base_url_node_admin;
      this.base_url_node_plain = this.loginAuthObj.base_url_node;
      this.base_url_node_only =  this.loginAuthObj.base_url_node;
      

      this.getqueryParam = {};
      this.getqueryParam = {"id":this.logged_in_user_id};
      this.getAdminProfile = this.base_url_node+"getAdminProfile"; 
      this._http.post(this.getAdminProfile,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getAdminProfile"+response);
      //this.apiResponse = response;
        this.record = response.record;

        const date = new Date();
        console.log("this.base_url"+this.base_url);
        var hours = Math.abs(date.getTime() - new Date(this.record.loggedin_time).getTime()) / 3600000;
        console.log("hours "+hours);
        if(hours > 24)
        {
          window.location.href = this.base_url+"logout";
        }

        /*console.log(this.record); 
        console.log(this.record.adminImage); */
        //console.log(this.record[0]);
        this.old_email = this.record.email;
        this.old_firstName = this.record.firstName;
        this.old_lastName = this.record.lastName;  
        this.old_id = this.record._id; 
        //this.old_userImage = this.record.adminImage; 
        this.old_userImage = this.base_url_node_plain+"public/uploads/adminProfile/"+this.record.adminImage;
        //console.log(this.old_userImage); 
        /*this.old_category = this.record[0].category;
        this.old_price = this.record[0].price;
        this.old_images = this.record[0].images;*/
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
        //console.log(this.old_category); 
      });
  }

  ngOnInit(): void {
  }

}
