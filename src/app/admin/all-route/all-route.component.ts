import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-route',
  templateUrl: './all-route.component.html',
  styleUrls: ['./all-route.component.css']
})
export class AllRouteComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allRoutePlan:any;queryParam:any;numbers:any;allRoutePlanCount:any;apiStringify:any;updateRoutePlanStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;
  selectedIndex: number;deleteRoutePlan:any;
  //dtOptions: DataTables.Settings = {}; 
  getqueryParam:any;assignToDriverApi:string='';

  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    //console.log(this.base_url_node);


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.exportCsvDriverApi = this.base_url_node+"exportCsvDriverApi";
    this.allRoutePlanCount = this.base_url_node+"allRoutePlanCount";
    this.allRoutePlan = this.base_url_node+"allRoutePlan";
    this.updateRoutePlanStatusApi = this.base_url_node+"updateRoutePlanStatusApi";
    this.deleteRoutePlan = this.base_url_node+"deleteRoutePlan";
    this.assignToDriverApi = this.base_url_node+"assignToDriverApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {


    // this.dtOptions = {
    //   "searching": false,
    //   "responsive": true,
    //   "autoWidth": false,
    //   "language": {
	  //   "search": "Recherche",
	  //   //"info": "Recherche",
	  //   //"entries": "Recherche",
	  //   }
    // };
    this._http.post(this.allRoutePlanCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allRoutePlanCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allRoutePlan,this.queryParam).subscribe((response:any)=>{
      //console.log("allRoutePlan"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }

  assignToDriver(route_id:any,driver_id:any)
  {
    console.log("route_id "+route_id);
    console.log("driver_id "+driver_id);
    this.getqueryParam = {"route_id":route_id,"driver_id":driver_id};
    this._http.post(this.assignToDriverApi,this.getqueryParam).subscribe((response:any)=>{
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        setTimeout(() => {
          window.location.reload();
        }, 2000); 
      }
    });
  }

  updateUserStatus(id=null,status:number)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateRoutePlanStatusApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  deleteDriverAC(id=null)
  {
    this.queryParam = {"id":id};
    this._http.post(this.deleteRoutePlan,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  exportCsv()
  {
    this.queryParam = {"pageNumber":0};
    this._http.get(this.exportCsvDriverApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
         
      }
    });
  }


  form = new UntypedFormGroup({
    fullName: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []), 
    status: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    console.log(this.form.value);
    
  }


}
