import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllBannerMiddleComponent } from './all-banner-middle.component';

describe('AllBannerMiddleComponent', () => {
  let component: AllBannerMiddleComponent;
  let fixture: ComponentFixture<AllBannerMiddleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllBannerMiddleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllBannerMiddleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
