import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoTimePickerComponent } from './demo-time-picker.component';

describe('DemoTimePickerComponent', () => {
  let component: DemoTimePickerComponent;
  let fixture: ComponentFixture<DemoTimePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoTimePickerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemoTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
