import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditZoneNameComponent } from './edit-zone-name.component';

describe('EditZoneNameComponent', () => {
  let component: EditZoneNameComponent;
  let fixture: ComponentFixture<EditZoneNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditZoneNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditZoneNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
