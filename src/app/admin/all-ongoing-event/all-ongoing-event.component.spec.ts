import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllOngoingEventComponent } from './all-ongoing-event.component';

describe('AllOngoingEventComponent', () => {
  let component: AllOngoingEventComponent;
  let fixture: ComponentFixture<AllOngoingEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllOngoingEventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllOngoingEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
