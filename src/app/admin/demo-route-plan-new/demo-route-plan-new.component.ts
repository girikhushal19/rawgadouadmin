import { Component, OnInit, ViewChild, ElementRef,AfterViewInit } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import {MapDirectionsService} from '@angular/google-maps';
import {Observable} from 'rxjs';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-demo-route-plan-new',
  templateUrl: './demo-route-plan-new.component.html',
  styleUrls: ['./demo-route-plan-new.component.css']
})
export class DemoRoutePlanNewComponent implements OnInit {
  
  markers:any[] = [];
  position:any;
  @ViewChild('map', {static: false}) mapElement:any= ElementRef;
  map:any;
  marker:any;
  directionsRendererccc:any;
  center = new google.maps.LatLng(22.7611714,75.8619856);
  html_view:string="";
  logged_in_user_id:any;
  queryParam:any;userPermissionRecord:any;base_url:any;base_url_node:any;base_url_node_plain:any;
  record:any;apiResponse:any;
  getAdminProfile:any;getqueryParam:any;
  totalDistance:number=0;totalTimeInMinute:number=0;totalTimeInHourMinute:number=0;hour:number=0;
  totalHour:number=0;totalMin:number=0;
  // mapOptions: google.maps.MapOptions = {
  //   center: this.center,
  //   zoom: 10,
  //   mapTypeId: google.maps.MapTypeId.ROADMAP
  // };
  
  ngAfterViewInit(){

    this.mapInit();
    const directionsService = new google.maps.DirectionsService();
    const directionsRenderer = new google.maps.DirectionsRenderer();
    

    this.getqueryParam = {"id":this.logged_in_user_id};
      this.getAdminProfile = this.base_url_node+"getAdminProfile"; 
      this._http.post(this.getAdminProfile,this.getqueryParam).subscribe((response:any)=>{
        
        if(response.error == false)
        {
          //console.log("api response "+response);
          var html = "";
              html += '<div class="stop-info-dialogue ui-widget-content ui-draggable ui-draggable-handle" id="modal-stop-detail" style="display: block;">';
              html += '<div class="stop-info-header d-flex justify-content-between align-items-cemnter" style="background-color: rgb(0, 0, 0);">';
                html += '<h6 class="stop_info_header">Stop No - 2 (Sadik)</h6>';
                html += '<ul class="d-flex align-items-center">';
                  html += '<li><a href="javascript:editStopReviewRoute(1798995);" title="Edit" class="editStop"><img src="https://cdncrew.upperinc.com/public/assets/img/stop-edit.svg" class="img-fluid" alt="Edit"></a></li>';
                  html += '<li><a href="javascript:beforeDelete(1798995);" title="Delete" class="deleteStop"><img src="https://cdncrew.upperinc.com/public/assets/img/delete-icon.svg" class="img-fluid" alt="Delete"></a></li>';
                  html += '<li><a href="#" title="Close" class="close-icon"><img src="https://cdncrew.upperinc.com/public/assets/img/stop-close.svg" class="img-fluid" alt="Close"></a></li>';
                html += '</ul>';
            html += '</div>';
            html += '<div class="stop-info-body">';
                html += '<ul class="d-flex flex-wrap">';
                  html += '<li>';
                      html += '<label>Address</label>';
                      html += '<p class="" id="view_stop_address" data-toggle="tooltip" data-placement="bottom" title="New Delhi, Delhi, India" data-original-title="">New Delhi, Delhi, India</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>ETA</label>';
                      html += '<p class="" id="view_stop_arrived_time">07:50 AM</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Nick Name</label>';
                      html += '<p class="" id="view_stop_v_nick_name"></p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Contact Name</label>';
                      html += '<p class="" id="view_stop_contact_name">-</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Company Name</label>';
                      html += '<p class="" id="view_stop_company_name">-</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Phone</label>';
                      html += '<p class="" id="view_stop_phone">-</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Email</label>';
                      html += '<p class="" id="view_stop_email">-</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Stop Type</label>';
                      html += '<p class="" id="view_stop_type">None</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Priority</label>';
                      html += '<p class="" id="view_stop_priority">Medium</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Notes</label>';
                      html += '<p class="" id="view_stop_notes" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">-</p>';
                  html += '</li>';
                  html += '<li>';
                      html += '<label>Time window</label>';
                      html += '<p class="" id="view_stop_time_window">-</p>';
                  html += '</li>';
                html += '</ul>';
              html += '</div>';
              html += '</div>';
          this.markers =
          [
            {
              "id":1,
              "timestamp": 'Vijay Nagar, Indore, Madhya Pradesh 452010, India',
              "description": '',
              "html":"html 1",
              "htmlView":html
            },
            {
              "id":2,
              "timestamp": 'Khajrana Ganesh Mandir, Ganeshpuri Main Road, Ganeshpuri, Khajrana, Indore, Madhya Pradesh, India',
              "description": '',
              "html":"html 2",
              "htmlView":html
            },
            {
              "id":3,
              "timestamp": 'New Father Angel H.S. School, Dubey Colony, Bajrang Nagar, Indore, Madhya Pradesh, India',
              "description": '',
              "html":"html 3",
              "htmlView":html
            },
            {
              "id":4,
              "timestamp": 'C21 Mall, AB Road, Scheme 54 PU4, Indore, Madhya Pradesh',
              "description": '',
              "html":"html 4",
              "htmlView":html
            },
            {
              "id":5,
              "timestamp": 'Saboro Mahindra Milk, LIG Colony, Indore, Madhya Pradesh',
              "description": '',
              "html":"html 5",
              "htmlView":html
            },
            {
              "id":6,
              "timestamp": 'Malwa Mill, Indore, Madhya Pradesh',
              "description": '',
              "html":"html 6",
              "htmlView":html
            },
            {
              "id":7,
              "timestamp": 'Durgesh Photo Framing, New Dewas Road, Patni Pura, Indore, Madhya Pradesh, India',
              "description": '',
              "html":"html 7",
              "htmlView":html
            },
            {
              "id":8,
              "timestamp": 'Utsav Hotel and Restaurant Dewas, AB Road, Opposite Laal Gate, Opposite State Bank Of India, Tekri Area, Itawa, Dewas, Madhya Pradesh, India',
              "description": '',
              "html":"html 8",
              "htmlView":html
            }
          ];
          this.calculateAndDisplayRoute(directionsService, directionsRenderer);
        }
      });

      /*
      */
  }  
  
  mapInit(){
    //this.map = new google.maps.Map(this.mapElement.nativeElement,
    //this.mapOptions);

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 6,
      center: { lat: 22.7611714, lng: 75.8619856 },
    });
    this.directionsRendererccc = new google.maps.DirectionsRenderer();
    this.directionsRendererccc.setMap(this.map);
  }

  calculateAndDisplayRoute(directionsService:any, directionsRenderer:any) {
    //console.log(this.markers[0]);
    const waypts = [];
    const checkboxArray = this.markers;
    //console.log("hereee 120")
    for (let i = 2; i < checkboxArray.length; i++)
    {
      waypts.push({
        location: checkboxArray[i].timestamp,
        stopover: true,
      });
    }
    //console.log("hereee waypts "+ JSON.stringify(waypts));
    
    directionsService
    .route({
      origin: 'Vijay Nagar, Indore, Madhya Pradesh 452010, India',
      destination: 'Khajrana Ganesh Mandir, Ganeshpuri Main Road, Ganeshpuri, Khajrana, Indore, Madhya Pradesh, India',
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
    }).then((response:any) => {

      //console.log("here 138")
      //console.log("response "+ JSON.stringify(response));
      //console.log(directionsRenderer);
      this.directionsRendererccc.setDirections(response);
      const route = response.routes[0];
      //console.log("route "+JSON.stringify(route));
      
      //console.log(route.waypoint_order);
      //console.log("response.routes[ 0 ].legs.length "+response.routes[ 0 ].legs.length);
      for(let k=0; k<response.routes[ 0 ].legs.length; k++)
      {
        //console.log("k "+k);
        console.log("route.legs[i].distance!.text "+JSON.stringify(route.legs[k].distance));
        //console.log("route.legs[i].distance!.text "+route.legs[k].distance!.text);
        console.log("route.legs[i].duration "+JSON.stringify(route.legs[k].duration));
        
        let distance_value = route.legs[k].distance!.value;
        distance_value = distance_value / 1000;
        let abc = Math.round(distance_value * 10) / 10;
        console.log("abc "+abc);
        this.totalDistance = this.totalDistance + abc;

        let duration_value = Number(route.legs[k].duration.value);
        duration_value = duration_value / 60;
        let xyz = duration_value.toFixed(0);
        this.totalTimeInMinute = this.totalTimeInMinute + parseFloat(xyz);
        console.log("xyz "+xyz);

        let m = k+1;
        let leg = response.routes[ 0 ].legs[ k ];
        //console.log(route.waypoint_order[k]);
        if(k > 0)
        {
          let o = route.waypoint_order[k-1];
          let p = o+2;
          //console.log("oooooo "+o);
          this.html_view = this.markers[p].htmlView;
          //console.log("this.html_view iffff"+this.html_view);
          //console.log("this.html_view iffff"+this.markers[o].id);
        }else{
          this.html_view = this.markers[k].htmlView;
          //console.log("this.html_view elseeee"+this.html_view);
        }

        this.makeMarker( leg.start_location, 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+m+'|000000|FFFFFF',response.routes[ 0 ].legs[ k ].start_address,this.html_view );

        if(m == response.routes[ 0 ].legs.length)
        {
          //console.log("iffff");
          let p = m+1;
          let mn = { lat: 22.7533404, lng: 75.8934695 };
          this.makeMarker( leg.end_location, 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+p+'|000000|FFFFFF',response.routes[ 0 ].legs[ k ].end_address,this.markers[1].htmlView );
        }

        //console.log(this.markers[k]);
        //console.log("checkboxArray --->   "+JSON.stringify(checkboxArray[k]));
        // console.log("id "+this.markers[k].id);
        // console.log("id "+this.markers[k].timestamp);
        // console.log(response.routes[ 0 ].legs[ k ]);
        // console.log(response.routes[ 0 ].legs[ k ].start_address);
        //console.log("leg.start_address "+response.routes[ 0 ].legs[ k ].start_address);
        //console.log("leg.end_location "+leg.end_location);
      }
      //console.log(" this.totalDistance "+ this.totalDistance);
      //console.log(" this.totalTimeInMinute "+ this.totalTimeInMinute);
      
      this.totalHour = Math.floor(this.totalTimeInMinute / 60);
      this.totalMin = this.totalTimeInMinute - (this.totalHour * 60);

      //console.log("this.totalHour "+this.totalHour);
      //console.log("this.totalMin "+this.totalMin)
      
      //console.log(response.routes[ 0 ].legs[ 0 ]);
      //console.log("sdfdf "+ JSON.stringify(response.routes[ 0 ].legs[ 0 ]));
    }).catch((e:any) => window.alert("Directions request failed due to " + e));
  }
  makeMarker( position:any, icon:any, title:any,htmlss:any )
  {
    const infowindow = new google.maps.InfoWindow({
      content: htmlss,
      ariaLabel: "Uluru",
    });
    const marker = new google.maps.Marker({
     position: position,
     map: this.map,
     icon:icon,
     title: title
    });
    marker.addListener("click", () => {
      infowindow.open({
        anchor: marker,
        map:this.map,
      });
    });
    
  }
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) {
    this.map= google.maps.Map;

    this.logged_in_user_id = localStorage.getItem("_id");
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    marker: google.maps.Marker;
  }
  ngOnInit() {
    //console.log(this.markers);
    //console.log(this.markers[0].latitude);
  }
  

  
  

}

