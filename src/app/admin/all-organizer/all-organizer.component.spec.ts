import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllOrganizerComponent } from './all-organizer.component';

describe('AllOrganizerComponent', () => {
  let component: AllOrganizerComponent;
  let fixture: ComponentFixture<AllOrganizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllOrganizerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllOrganizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
