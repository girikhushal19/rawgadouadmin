import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllInvoiceShipmentComponent } from './all-invoice-shipment.component';

describe('AllInvoiceShipmentComponent', () => {
  let component: AllInvoiceShipmentComponent;
  let fixture: ComponentFixture<AllInvoiceShipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllInvoiceShipmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllInvoiceShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
