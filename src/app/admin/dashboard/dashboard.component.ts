import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { Chart, registerables } from 'chart.js';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  homedata: any;
  labeldays: any;
  totalsales: any;
  totalorders: any;
  totalcommissions: any
  orderpie: any;deliverpie:any;
  base_url:any;
  ordersthismonth:number = 0;totalcommissionearned:number = 0;totalsellerthismonth:number = 0;
  sellerthismonthnotapproved:number = 0;totalproductsthismonth:number = 0;totalwithdrawls:number = 0;
  selectedIndex: number;queryParam:any;record:any;base_url_node:string="";
  allAdminShipment:any; numbers:any;allAdminShipmentCount:any;apiStringify:any;totalPageNumber:any;
  apiResponse:any;
  constructor(private http: HttpClient,private loginAuthObj:LoginauthenticationService) {
    this.homedata = []
    this.labeldays = []
    this.totalsales = []
    this.totalorders = []
    this.totalcommissions = []
    this.orderpie = []; this.deliverpie = [];
    this.base_url = loginAuthObj.baseapiurl;
    this.selectedIndex = 0;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.allAdminShipmentCount = this.base_url_node+"allLatestShipmentCount";
    this.allAdminShipment = this.base_url_node+"allLatestShipment";
    this.http.post(this.allAdminShipmentCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allAdminShipmentCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
    this.getallBanner(0);
  }

  ngOnInit(): void {
    this.Renderchart()

  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this.http.post(this.allAdminShipment,this.queryParam).subscribe((response:any)=>{
      //console.log("allAdminShipment"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  Renderchart() {

    this.http.get(this.base_url).subscribe(res => {
      this.homedata = res;
      this.ordersthismonth = this.homedata.data.ordersthismonth.length;
      this.totalcommissionearned = this.homedata.data.totalcommissionearned;
      this.totalsellerthismonth = this.homedata.data.totalsellerthismonth;
      this.sellerthismonthnotapproved = this.homedata.data.sellerthismonthnotapproved;
      this.totalproductsthismonth = this.homedata.data.totalproductsthismonth;
      this.totalwithdrawls = this.homedata.data.totalwithdrawls;

      this.labeldays = this.homedata.data.numberofdaysarray
      this.totalsales = this.homedata.data.totalsalesarray
      this.totalcommissions = this.homedata.data.totalcommissionarray
      this.totalorders = this.homedata.data.totalordersarray
      this.orderpie = this.homedata.data.orderspie;
      this.deliverpie = this.homedata.data.deliverpie;
      console.log("this.deliverpie "+this.deliverpie);
      new Chart('linechart', {
        type: 'line',
        data: {
          labels: this.labeldays,
          datasets: [{
            label: 'Ventes totales',
            data: this.totalsales,
            borderWidth: 1,
            fill: false,
            borderColor: 'rgb(0, 143, 251)',
            tension: 0.1
          },
          {
            label: 'Total des commandes',
            data: this.totalorders,
            borderWidth: 1,
            fill: false,
            borderColor: 'rgb(254, 176, 25)',
            tension: 0.1
          },
          {
            label: 'Total des commissions',
            data: this.totalcommissions,
            borderWidth: 1,
            fill: false,
            borderColor: 'rgb(119, 93, 208)',
            tension: 0.1
          }
          ]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
              grid: {
                display: false,
              },
            },
            x: {
              grid: {
                display: false,
              },
            }
          }
        }
      });

      new Chart('piechart', {
        type: 'doughnut',
        data: {
          labels: [
            'Total',
            'Terminé',
            'Traitement',
            'Annulés',
            'Remboursé',
          ],
          datasets: [{
            label: 'commandes',
            data: this.orderpie,

            backgroundColor: [
              '#2171ae',
              '#28a745',
              '#ffc107',
              'rgb(255, 69, 96)',
              '#17a2b8'
            ],
            hoverOffset: 4
          }]
        },
        options: {
         
          plugins: {
            legend: {
              position: 'left'
            }

          }

        }
      });

      new Chart('polar', {
        type: 'doughnut',
        data: {
          labels: [
            'Total',
            'En cours',
            'Expédié',
            'Terminé',
            'Annulé',

          ],
          datasets: [{
            label: 'Dépenses',
            data: this.deliverpie,
            backgroundColor: [
              'rgb(0, 143, 251)',
              'rgb(254, 176, 25)',
              'rgb(255, 69, 96)',
              'rgb(119, 93, 208)',
              'rgb(0, 216, 182)'
            ],
            hoverOffset: 4
          }]
        },
        options: {
          
          plugins: {
            legend: {
              position: 'left'
            },
            
          }

        }
      });
    })




  }

}
