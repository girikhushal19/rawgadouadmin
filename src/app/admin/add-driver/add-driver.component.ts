import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.css']
})
export class AddDriverComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addDriverSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;

  default_date: Date = new Date(); default_date2: Date = new Date();
  default_date3: Date = new Date(); default_date4: Date = new Date();
  //ddDate = new Date('Thu Jan 26 2023 22:38:32 GMT+0530 (India Standard Time)');
  startAddress: string = '';startLatitude: string = '';startLongitude: string = '';
  endAddress: string = '';endLatitude: string = '';endLongitude: string = '';
  allFormValue:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      //window.location.href = this.base_url;
    }
    this.addDriverSubmit = this.base_url_node+"addDriverSubmit";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    
  }
  ngOnInit(): void {
      this.myFiles = []; 
      // console.log(this.ddDate);
      // console.log(this.ddDate.getHours());
      // console.log(this.ddDate.getMinutes());
      // console.log(this.ddDate.getSeconds());
  }

  handleAddressChange(address: any)
  {
    //console.log(address);
    this.startAddress = address.formatted_address
    this.startLatitude = address.geometry.location.lat()
    this.startLongitude = address.geometry.location.lng()
  }
  handleAddressChange2(address2: any)
  {
    //console.log(address2);
    this.endAddress = address2.formatted_address
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
  }

  form = new UntypedFormGroup({
    fullName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required]),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    password: new UntypedFormControl('', [Validators.required]),
    vehicleType: new UntypedFormControl('', [Validators.required]),
    shiftStartTime: new UntypedFormControl('', []),
    shiftEndTime: new UntypedFormControl('', []),
    breakStartTime: new UntypedFormControl('', []),
    breakEndTime: new UntypedFormControl('', []),
    startAddress: new UntypedFormControl('', [Validators.required]),
    endAddress: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      console.log(this.form.value);
      this.allFormValue = {
        "fullName":this.form.value.fullName,
        "email":this.form.value.email,
        "mobileNumber":this.form.value.mobileNumber,
        "password":this.form.value.password,
        "vehicleType":this.form.value.vehicleType,
        "shiftStartTime":this.form.value.shiftStartTime,
        "shiftEndTime":this.form.value.shiftEndTime,
        "breakStartTime":this.form.value.breakStartTime,
        "breakEndTime":this.form.value.breakEndTime,
        "startAddress":this.startAddress,
        "startLatitude":this.startLatitude,
        "startLongitude":this.startLongitude,
        "endAddress":this.endAddress,
        "endLatitude":this.endLatitude,
        "endLongitude":this.endLongitude,
      };
      
      this._http.post(this.addDriverSubmit,this.allFormValue).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          //this.form.reset();
          //this.myFiles = [];
          setTimeout(() => {
            window.location.reload();
          }, 2000); 
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
