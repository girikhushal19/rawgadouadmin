import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { AddadvertiseComponent } from '../addadvertise/addadvertise.component';
import { HttpClient } from '@angular/common/http';
import { LoginauthenticationService } from '../../adminservice/loginauthentication.service';
@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.css']
})
export class AdvertisementComponent implements OnInit {
  base_url: any; ordersarray:any; orders:any; p:any;
  page_no:number=0;numbers:any;selectedIndex: number=0;

  constructor(public dialog: MatDialog,private http: HttpClient, private loginAuthObj: LoginauthenticationService) {
    this.base_url = loginAuthObj.baseapiurl
    this.ordersarray =[]
    this.orders =[]

     

    this.http.get(this.base_url + "getallannouncements/null").subscribe(res=>{
      console.log("res "+res);
      this.ordersarray = res
      this.page_no = this.ordersarray.pages;
        this.numbers = Array(this.page_no).fill(0).map((m,n)=>n);
        
     // console.log(this.orders)
    })
    this.getallColor(0)
   }

  ngOnInit(): void {}
  getallColor(pageno:number=0)
  {
    this.http.get(this.base_url + "getallannouncements/"+pageno).subscribe(res=>{
      console.log("res "+res);
      this.ordersarray = res
      this.orders = this.ordersarray.data
     // console.log(this.orders)
    })
  }
  select_all = false;
  data: any[] = [
    { id: 1, Title: 'Info -Demo1', is_selected: false ,Content:'Content for 1',Sent_to:'All Vendors',Date:'Feb 12, 2022',status:'Published',achats:'4'},
    { id: 2, Title: 'Demo2', is_selected: false ,Content:'Content for 2',Sent_to:'All Vendors',Date:'Jan 06, 2022',status:'Published',achats:'3'},
    { id: 3, Title: 'Demo3', is_selected: false ,Content:'Content for 3',Sent_to:'All Vendors',Date:'Feb 23, 2022',status:'Published',achats:'7'}
  ]
  onSelectAll(e: any): void { 
    for (let i = 0; i < this.data.length; i++) {
      const item = this.data[i];
      item.is_selected = e;
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddadvertiseComponent ,{
      height: '400px',
      width: '800px'
    });
  }
}
