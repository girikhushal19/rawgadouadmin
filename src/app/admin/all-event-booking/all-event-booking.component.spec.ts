import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEventBookingComponent } from './all-event-booking.component';

describe('AllEventBookingComponent', () => {
  let component: AllEventBookingComponent;
  let fixture: ComponentFixture<AllEventBookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllEventBookingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllEventBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
